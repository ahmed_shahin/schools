-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2018 at 05:33 PM
-- Server version: 5.6.20
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `schools`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_details` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `en_details` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `ar_details`, `en_details`, `created_at`, `updated_at`) VALUES
(1, 'الشروط العامة:-\r\n\r\n1- أن يكون المتقدم أو المتقدمة سعودي الجنسية.\r\n\r\n2- أن تكون شهادة الدكتوراه (وقرار الترقية للمتقدمين على درجة أستاذ مشارك أو أستاذ) من جامعة سعودية أو جامعة أخرى معترف بها.\r\n\r\n3- أن يكون تخصص الدكتوراة امتداد لتخصص البكالوريوس والماجستير.\r\n\r\n4- اجتياز المقابلة الشخصية والاختبارات التي تجريها الأقسام العلمية وتقديم محاضرة علمية للمرشحين النهائيين .\r\n', 'From the vision of the Kingdom 2030, and in order to contribute to this vision, we will give you a Waffer card that will give you discounts from all your daily interests. We strive to make you spare from your daily allowance and for a whole year we will be at your service anytime during the official hours trying to make a cheek', '2018-01-30 22:00:00', '2018-02-19 09:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'ادمن', 'admin@admin.com', '$2y$10$7SHwH185VCnqOfI3rogJNetTI4MtsH/iPlj0lqrjDpaXsZ7X.gMEu', '2018-01-30 22:00:00', '2018-04-22 06:09:58'),
(2, 'عمرو', 'amrmehana47@gmail.com', '$2y$10$PxHYVioaqSThwhtyd39PheQ6LWbzWY1rF3O3hiWzldUXNxz9LQDgG', '2018-01-30 22:00:00', '2018-02-21 18:28:01'),
(3, 'اكسترا', 'extra4it@gmail.com', '$2y$10$7SHwH185VCnqOfI3rogJNetTI4MtsH/iPlj0lqrjDpaXsZ7X.gMEu', '2018-01-30 22:00:00', '2018-02-19 11:13:03'),
(7, 'حاتم', 'hatem@gmail.com', '$2y$10$7SHwH185VCnqOfI3rogJNetTI4MtsH/iPlj0lqrjDpaXsZ7X.gMEu', '2018-02-14 06:59:55', '2018-02-14 06:59:55'),
(9, 'اكسترا ', 'extra4itapps@gmail.com', '$2y$10$2wgpmgk4SbpuZKqVuWGl9OgtCvqRwS1Xunwd5pVtG26MGWn.vb2rS', '2018-03-21 05:49:02', '2018-03-21 05:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `national_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `card_type_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `name`, `code`, `phone`, `email`, `address`, `national_id`, `status`, `card_type_id`, `created_at`, `updated_at`) VALUES
(1, 'amr', 'pPeKZL11E7', '888', 'amrmehana47@@gmail.com', 'egypt', '87848', 0, 1, '2018-02-06 15:38:45', '2018-02-06 15:38:45'),
(2, 'ali', '1Zy0bICtir', '9885', 'amrmehana47@@gmail.com', 'mansoura', '85585', 1, 1, '2018-02-06 15:47:00', '2018-02-25 15:06:53'),
(3, 'amr', '8Dtdk3mjZz', '458', 'amrmehana47@@gmail.com', 'egypt mansoura', '84858', 0, 1, '2018-02-06 19:25:49', '2018-02-06 19:25:49'),
(4, 'ahmed', 'S6Slmfcnwz', '5555', 'amrmehana47@@gmail.com', 'saudia', '585', 0, 1, '2018-02-08 18:48:28', '2018-02-08 18:48:28'),
(5, 'mohamed elgohhary', 'Ykd1319CIL', '1063663653', 'mohamedelgohaey933@gmail.com', 'emarats', '255488229632', 0, 1, '2018-02-13 14:07:26', '2018-02-13 14:07:26'),
(6, 'mohamed', 'o4PwKRNltM', '888', 'extra4itapps@gmail.com', 'dobai', '8888', 0, 1, '2018-03-12 17:43:51', '2018-03-12 17:43:51');

-- --------------------------------------------------------

--
-- Table structure for table `card_types`
--

CREATE TABLE `card_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `card_types`
--

INSERT INTO `card_types` (`id`, `ar_name`, `en_name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'بطاقة الافراد 200', 'personal card 200', 200, '2018-02-01 07:58:36', '2018-02-01 07:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(1, 'الدمام', 'damam', '2018-01-31 14:59:06', '2018-01-31 14:59:06'),
(2, 'المدينة', 'al madinah', '2018-02-04 11:22:24', '2018-02-04 11:22:24'),
(3, 'حفر الباطن', 'Hafaralbatin', '2018-02-10 09:25:07', '2018-02-10 09:25:07');

-- --------------------------------------------------------

--
-- Table structure for table `classrooms`
--

CREATE TABLE `classrooms` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `grade_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classrooms`
--

INSERT INTO `classrooms` (`id`, `ar_name`, `en_name`, `grade_id`, `created_at`, `updated_at`) VALUES
(1, 'ثاني روضة', 'second roda', 2, '2018-05-02 09:12:45', '2018-05-02 07:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commercial_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `location_details` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `officer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `officer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `officer_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `officer_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contract` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commercial_license` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ar_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `ar_name`, `en_name`, `commercial_no`, `owner_name`, `owner_phone`, `owner_email`, `owner_fax`, `country`, `city`, `street`, `location_details`, `officer_name`, `officer_email`, `officer_phone`, `officer_fax`, `contract`, `commercial_license`, `bank_data`, `ar_logo`, `en_logo`, `created_at`, `updated_at`) VALUES
(1, 'التميز', 'tamioz', '3415465', '', '', '', '', 'السعودية', 'الرياض', 'الرياض', 'لىبلنت بتلىتبانتلبنتي ىي بتنيىستب يبىىسينتىبتثسي نبىسيمبنتسم نستيسشنيشس سشتينصكسمن شسيمصشسيمنشسي نيسشببسشتب بننستبنمس سبىمنيستبمؤ', '', '', '', '', '', NULL, '', 'uploads/BiBzVFB1jM0ktEwFsrET.jpeg', 'uploads/TBHPy9tHgjQ4CWnSFpJo.jpeg', '2018-03-30 21:57:18', '2018-03-30 21:57:18'),
(2, 'الاصدقاء', 'friends', '9876543', 'amr', '01022162396', 'amrmehana47@gmail.com', '9845646', 'السعودية', 'المدينة', 'المدينة المدينة', 'ؤبلا الاعهلغل الهعهغ الرعبقفثيق الربءقئيءبؤلارا تالبلتانت تاالبالاة ى العتلتن اؤغفبعتى تالعغؤفغلى ابغيبغعتا ؤفغلرعاتىة  تارفقغبالتاى', 'ahmed', 'ahmed@gmail.com', '164654546598', '454545', 'uploads/ZhodJSNfTMhAamAZV59i.pdf', 'uploads/kIZ3Sks12tu3luppnLEJ.pdf', 'uploads/9MGXbVaswaVQz0PKgR7W.pdf', 'uploads/5ZOL8Lvabgqwi3WAsETw.jpeg', 'uploads/wOjyKLEsrvHFPQXlLKOs.jpeg', '2018-03-30 22:00:35', '2018-03-31 13:43:18'),
(3, 'شركه الأمل العالميه ', 'Amal Company', '01000', 'محمد ال سعود', '0500000000', 'w.gh@gmail.com', '', 'السعوديه', 'الرياض', 'الرمال', 'شارع الحسن بن علي المتفرع من الملك فهد ', 'عامر', '', '0535454545', '', 'uploads/HGWSQcKrydrUYa9rugco.pdf', 'uploads/GNI0L5ZLDfvhG3UR9Qfl.jpeg', 'uploads/vc4PdL2sdGuBzD7PLg9D.jpeg', 'uploads/ZXS9GPnxlorsqceagBgQ.jpeg', 'uploads/tvYLv61e7hSrMKo5ziuZ.jpeg', '2018-04-01 21:29:10', '2018-04-01 21:29:10');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_details` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `en_details` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `ar_details`, `en_details`, `created_at`, `updated_at`) VALUES
(1, '<p style=\"text-align:right\">الشروط العامة :-&nbsp;</p>\r\n\r\n<p style=\"text-align:right\">1- أن يكون المتقدم أو المتقدمة سعودي الجنسية.&nbsp;</p>\r\n\r\n<p style=\"text-align:right\">2- أن تكون شهادة الدكتوراه (وقرار الترقية للمتقدمين على درجة أستاذ مشارك أو أستاذ) من جامعة سعودية أو جامعة أخرى معترف بها.</p>\r\n\r\n<p style=\"text-align:right\">3- أن يكون تخصص الدكتوراة امتداد لتخصص البكالوريوس والماجستير.</p>\r\n\r\n<p style=\"text-align:right\">4- اجتياز المقابلة الشخصية والاختبارات التي تجريها الأقسام العلمية وتقديم محاضرة علمية للمرشحين النهائيين .</p>\r\n', 'features features\r\n', '2018-01-30 22:00:00', '2018-02-14 07:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `photo`, `user_id`, `created_at`, `updated_at`) VALUES
(4, 'uploads/0MdePAh7M7.jpeg', 2, '2017-12-30 12:04:13', '2017-12-30 12:04:13'),
(5, 'uploads/Ffxmi8j1cK.jpg', 2, '2017-12-30 12:07:36', '2017-12-30 12:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `masters`
--

CREATE TABLE `masters` (
  `id` int(11) NOT NULL,
  `fathername` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `relation_type` varchar(255) NOT NULL,
  `student_id` int(11) UNSIGNED DEFAULT NULL,
  `country_id` int(11) UNSIGNED DEFAULT NULL,
  `city_id` int(11) UNSIGNED DEFAULT NULL,
  `street` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `masters`
--

INSERT INTO `masters` (`id`, `fathername`, `phone`, `email`, `relation_type`, `student_id`, `country_id`, `city_id`, `street`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'شاهينشش', 1147498989, 'shahin@gmail.com', '0', 13, 0, 0, 'sdsdsadssdsa', 'shahin', '$2y$10$2K.10liDYqwmF2cUUPXAn.W5wTcj09/5BA1C7DaWFL3kkpq6Rh7Am', '2018-05-03 12:52:18', '2018-05-03 13:14:08');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `phone`, `details`, `created_at`, `updated_at`) VALUES
(1, 'amrr', 'amrr@gmail.com', '01022162396', 'help me ', '2018-02-05 09:09:48', '2018-02-05 09:09:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_card_typies_table', 1),
('2014_10_12_000000_create_cards_table', 1),
('2014_10_12_000000_create_cities_table', 1),
('2014_10_12_000000_create_sections_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_22_115189_create_settings_table', 1),
('2016_06_22_160118_create_messages_table', 1),
('2016_06_22_164508_create_abouts_table', 1),
('2016_06_22_164508_create_features_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `passwords`
--

CREATE TABLE `passwords` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `passwords`
--

INSERT INTO `passwords` (`id`, `email`, `code`) VALUES
(1, 'amrmehana47@gmail.com', 'iq3YKp'),
(2, 'amrmehana47@gmail.com', 'MimTpD'),
(3, 'amrmehana47@gmail.com', 'AUTGOX'),
(4, 'amrmehana47@gmail.com', 'bICZTD'),
(5, 'amrmehana47@gmail.com', 'm76Vtt'),
(6, 'amrmehana47@gmail.com', '1X5b9Q'),
(7, 'amrmehana47@gmail.com', 'Q5DiKL'),
(8, 'amrmehana47@gmail.com', 'KfyiEz'),
(9, 'amrmehana47@gmail.com', 'M74ewR'),
(10, 'amrmehana47@gmail.com', 'Em1Fty'),
(11, 'amrmehana47@gmail.com', 'Uzmy1F'),
(12, 'amrmehana47@gmail.com', 'pWX6jO'),
(13, 'amrmehana47@gmail.com', 'BLgv8h'),
(14, 'amrmehana47@gmail.com', 'EFXeFd'),
(15, 'amrmehana47@gmail.com', 'nEed19'),
(16, 'amrmehana47@gmail.com', 'u2AOv5'),
(17, 'amrmehana47@gmail.com', 'tBIFFx'),
(18, 'amrmehana47@gmail.com', 'pfEceZ'),
(19, 'amrmehana47@gmail.com', 'Rpmln4');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manager_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manager_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manager_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manager_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secretary_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secretary_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secretary_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secretary_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `location_details` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `officer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `officer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `officer_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `officer_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stages` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `specialization` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curriculum_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  ` students_gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ar_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ar_logo_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_logo_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `ar_name`, `en_name`, `company_id`, `email`, `password`, `manager_name`, `manager_phone`, `manager_email`, `manager_fax`, `secretary_name`, `secretary_phone`, `secretary_email`, `secretary_fax`, `agent_name`, `agent_phone`, `agent_email`, `agent_fax`, `country`, `city`, `street`, `location_details`, `lat`, `lang`, `officer_name`, `officer_email`, `officer_phone`, `officer_fax`, `stages`, `specialization`, `curriculum_type`, ` students_gender`, `ar_logo`, `en_logo`, `ar_logo_text`, `en_logo_text`, `created_at`, `updated_at`) VALUES
(2, 'مدرسه الامل العالميه بنين وبنات', 'Al amal intarnational School ', 3, 'walid@gmail.com', '$2y$10$7SHwH185VCnqOfI3rogJNetTI4MtsH/iPlj0lqrjDpaXsZ7X.gMEu', 'وليد غنيم', '0551078711', 'walidghonem@gmail.com', '', 'محمود', '011111111', '', '', 'احمد ', '0222222222', '', '', 'السعوديه', 'الرياض', 'الرمال', 'شارع الحسن بن علي المتفرع من الملك فهد ', '', '', 'خالد', '', '0333333333', '', 'boys_girls', 'international', 'american', '', 'uploads/GpgbVyRSTizeHCJVcb3r.jpeg', 'uploads/gaNfjyWWHxIShwxG7Ber.jpeg', 'الامل العالميه', 'Alamal School', '2018-04-01 21:43:49', '2018-04-01 21:43:49');

-- --------------------------------------------------------

--
-- Table structure for table `school_grades`
--

CREATE TABLE `school_grades` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) DEFAULT NULL,
  `en_name` varchar(255) DEFAULT NULL,
  `manager` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `office` varchar(255) NOT NULL,
  `transfer` varchar(255) NOT NULL,
  `school_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school_grades`
--

INSERT INTO `school_grades` (`id`, `ar_name`, `en_name`, `manager`, `phone`, `email`, `office`, `transfer`, `school_id`, `created_at`, `updated_at`) VALUES
(2, 'الابتداي', 'primary', 'ahmed', 1114749989, 'shahin@gmail.com', 'shahin office', 'ahmed shahin', 2, '2018-05-01 09:08:18', '2018-05-01 11:09:43');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `ar_name`, `en_name`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'صيدليات', 'pharmacy', 'uploads/qhEcNlhuwU.png', '2018-02-01 09:44:11', '2018-02-01 09:44:11');

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `classroom_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `ar_name`, `en_name`, `classroom_id`, `created_at`, `updated_at`) VALUES
(2, 'الاول', 'الثاني', 1, '2018-05-02 07:55:42', '2018-05-02 12:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `email1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `snap` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `email1`, `email2`, `phone1`, `phone2`, `description`, `keywords`, `facebook`, `twitter`, `linkedin`, `youtube`, `google`, `snap`, `whatsapp`, `instagram`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'momiz@gmail.com', 'momiz@gmail.com', '0506660773', '096502564212', 'momiz momiz momiz', 'momiz momiz momiz', 'facebook', '', '', 'youtube', 'google', 'snap', 'whatsapp', 'instagram', '', '2018-01-30 22:00:00', '2018-02-20 07:55:52');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `enrollmentdate` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `classroom_id` int(11) DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `img`, `birthdate`, `enrollmentdate`, `gender`, `grade_id`, `classroom_id`, `semester_id`, `created_at`, `updated_at`) VALUES
(1, 'adsadsa', 'uploads/IYqKzTUopiPyRiIlrD9f.jpeg', '2018-05-17', '2018-05-04', 'انثى', 2, 1, 2, '2018-05-02 22:00:00', '2018-05-03 09:45:21'),
(2, 'sadsa', 'uploads/x2f5uAKu7jMme7OwlbrU.jpeg', '2018-05-03', '2018-05-04', '', 2, 1, 2, '2018-05-03 08:56:03', '2018-05-03 08:56:03'),
(3, 'sadsa', 'uploads/SrjvDCSiPtxeg1O8kUH4.jpeg', '2018-05-03', '2018-05-05', '', 2, 1, 2, '2018-05-03 08:58:35', '2018-05-03 08:58:35'),
(4, 'sdsda', 'uploads/qvKMJsQPwHOx12EbwyY5.jpeg', '2018-05-03', '2018-05-11', '0', 2, 1, 2, '2018-05-03 09:03:35', '2018-05-03 09:03:35'),
(5, 'hgfhj', 'uploads/4JGjVQMyMnYb4QANS5tK.jpeg', '2018-05-17', '2018-05-19', '0', 2, 1, 2, '2018-05-03 09:05:50', '2018-05-03 09:05:50'),
(6, 'sdfghj', 'uploads/jI4thTLIAeDM4Q7tcqWt.jpeg', '2018-05-01', '2018-05-12', '1', 2, 1, 2, '2018-05-03 09:08:26', '2018-05-03 09:08:26'),
(7, 'asadsa', 'uploads/DI2AoivBu43jLqoC6NBU.jpeg', '2018-05-17', '2018-05-11', 'ذكر', 2, 1, 2, '2018-05-03 09:10:57', '2018-05-03 09:43:24'),
(8, 'sadfg', 'uploads/4OOfO1DUTe8djqMjn9p4.jpeg', '2018-05-04', '2018-05-17', 'انثى', 2, 1, 2, '2018-05-03 09:12:47', '2018-05-03 09:12:47'),
(9, 'csdsa', 'uploads/O1BUV5ABvGTZ0rAlrQov.jpeg', '2018-05-05', '2018-05-26', 'ذكر', 2, 1, 2, '2018-05-03 11:57:09', '2018-05-03 11:57:09'),
(10, 'sdsa', 'uploads/yZgQVzbcvlsizniazUfY.jpeg', '2018-05-10', '2018-05-11', 'ذكر', 2, 1, 2, '2018-05-03 11:57:39', '2018-05-03 11:57:39'),
(11, 'احمد', 'uploads/T5uX5bVAQIMpTuygUoQX.jpeg', '2018-05-10', '2018-05-05', 'ذكر', 2, 1, 2, '2018-05-03 12:49:15', '2018-05-03 12:49:15'),
(12, 'احمد', 'uploads/VG13jPaIjhSmnhs1tXfL.jpeg', '2018-05-10', '2018-05-05', 'ذكر', 2, 1, 2, '2018-05-03 12:49:43', '2018-05-03 12:49:43'),
(13, 'احمديسشي', 'uploads/XwMkKQti1ztznuXtosgl.jpeg', '2018-05-10', '2018-05-05', 'ذكر', 2, 1, 2, '2018-05-03 12:52:18', '2018-05-03 13:14:08');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `ar_img` varchar(255) DEFAULT NULL,
  `en_img` varchar(255) DEFAULT NULL,
  `grade_id` int(11) UNSIGNED DEFAULT NULL,
  `classroom_id` int(11) UNSIGNED DEFAULT NULL,
  `semester_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `ar_name`, `en_name`, `ar_img`, `en_img`, `grade_id`, `classroom_id`, `semester_id`, `created_at`, `updated_at`) VALUES
(1, 'dfds', 'sddsf', 'E:\\xampp\\tmp\\php3F21.tmp', 'E:\\xampp\\tmp\\php3F42.tmp', 2, 1, 2, '2018-05-03 05:49:02', '2018-05-03 05:49:02'),
(2, 'sdsadsa', 'sadsa', 'E:\\xampp\\tmp\\phpF632.tmp', 'E:\\xampp\\tmp\\phpF633.tmp', 2, 1, 2, '2018-05-03 05:51:59', '2018-05-03 05:51:59'),
(3, 'sadsa', 'sadsa', 'E:\\xampp\\tmp\\php1D73.tmp', 'E:\\xampp\\tmp\\php1D74.tmp', 2, 1, 2, '2018-05-03 05:56:31', '2018-05-03 05:56:31'),
(4, 'sdsad', 'sadsa', 'E:\\xampp\\tmp\\phpC8E0.tmp', 'E:\\xampp\\tmp\\phpC8F1.tmp', 2, 1, 2, '2018-05-03 06:00:32', '2018-05-03 06:00:32'),
(5, 'سيشسيشس', 'سيسشس', 'uploads/uIp0aBN9rFaP0Pbsaw7g.jpeg', 'uploads/TIaNs5T7rLN6kwMs4BOF.jpeg', 2, 1, 2, '2018-05-03 06:04:45', '2018-05-03 06:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ar_details` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `en_details` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `device_token` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ar_name`, `en_name`, `email`, `password`, `lat`, `lang`, `photo`, `phone`, `address`, `ar_details`, `en_details`, `discount`, `type`, `section_id`, `city_id`, `device_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ادمن', 'admin', 'admin@admin.com', '$2y$10$7SHwH185VCnqOfI3rogJNetTI4MtsH/iPlj0lqrjDpaXsZ7X.gMEu', '64865', '4512', '', '01022162396', 'mansoura', 'mansoura', '', '10', 'admin', 1, 1, '', '9O4W1pJ5WnJ3Y3LLqIRyrrwtJgDymKIvdPplR5tEj54t2BnglCz7v4J69kgI', '2018-01-30 22:00:00', '2018-04-22 06:09:58'),
(2, 'عمرو', 'amr', 'amrmehana47@gmail.com', '$2y$10$PxHYVioaqSThwhtyd39PheQ6LWbzWY1rF3O3hiWzldUXNxz9LQDgG', '64865', '4512', 'uploads/mKzu09mqb7.png', '٠١٦٦٦٦٦٦٦٦٦٦٦', 'mansoura', '<p>خصم ٤٠٪ على قسم الاشعة خصم ٤٠٪ على قسم الاسنان خصم ٤٠٪ على قسم الجلدية</p>\r\n', '<p>details</p>\r\n', '10', 'shop', 1, 1, '', 'b1sbWwXePcfYnYvOYNnEtuSf9bKHZCquyDEdQsmdwnkl2oApmcZERIPGzhkG', '2018-01-30 22:00:00', '2018-02-21 18:28:01'),
(3, 'اكسترا', 'extra', 'extra4it@gmail.com', '$2y$10$7SHwH185VCnqOfI3rogJNetTI4MtsH/iPlj0lqrjDpaXsZ7X.gMEu', '64865', '4512', '', '01022162396', 'mansoura', '<p style=\"text-align:right\"><span style=\"color:#2ecc71\"><strong>mansoura</strong></span></p>\r\n', '', '10', 'shop', 1, 2, '', 'b1sbWwXePcfYnYvOYNnEtuSf9bKHZCquyDEdQsmdwnkl2oApmcZERIPGzhkG', '2018-01-30 22:00:00', '2018-02-19 11:13:03'),
(7, 'حاتم', 'hatem', 'hatem@gmail.com', '$2y$10$7SHwH185VCnqOfI3rogJNetTI4MtsH/iPlj0lqrjDpaXsZ7X.gMEu', '27.57855496065794', '46.172656250000045', 'uploads/p7D1RaQ311.jpg', '01022162396', 'djngjds dskgnljsd', 'fmvkfgv fsgjdskdjgf', 'sdlkjfslkdj sdknglfkjhdlgfj kjsdfhkl', '10', 'shop', 1, 3, '', NULL, '2018-02-14 06:59:55', '2018-02-14 06:59:55'),
(9, 'اكسترا ', 'extra apps', 'extra4itapps@gmail.com', '$2y$10$2wgpmgk4SbpuZKqVuWGl9OgtCvqRwS1Xunwd5pVtG26MGWn.vb2rS', '26.953559778172426', '29.297656250000045', 'uploads/R0w2Zs06M5.png', '01022162396', 'mansoura', '<p>company for programming</p>\r\n', '<p>company for programming</p>\r\n', '10', 'shop', 1, 1, '', NULL, '2018-03-21 05:49:02', '2018-03-21 05:49:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_types`
--
ALTER TABLE `card_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classrooms`
--
ALTER TABLE `classrooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade_id` (`grade_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`commercial_no`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masters`
--
ALTER TABLE `masters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passwords`
--
ALTER TABLE `passwords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_grades`
--
ALTER TABLE `school_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classroom_id` (`classroom_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade_id` (`grade_id`),
  ADD KEY `classroom_id` (`classroom_id`),
  ADD KEY `semester_id` (`semester_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semester_id` (`semester_id`),
  ADD KEY `grade_id` (`grade_id`),
  ADD KEY `classroom_id` (`classroom_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `card_types`
--
ALTER TABLE `card_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `classrooms`
--
ALTER TABLE `classrooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `masters`
--
ALTER TABLE `masters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `passwords`
--
ALTER TABLE `passwords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `school_grades`
--
ALTER TABLE `school_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
