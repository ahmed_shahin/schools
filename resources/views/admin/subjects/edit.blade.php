@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">المواد الدراسية</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">تعديل المادة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> تعديل المادة الدراسية
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'put','url'=>'school/subjects/'.$subjects->id ,'role'=>"form",'files'=>'true']) !!}
             <center><b>بيانات الفصل الدراسي</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
            <div class="row">
              <label class="col-md-2 control-label">‫ المرحلة  الدراسية </label>
            <div class="col-md-10">
          {!! Form::select('grade_id',$grades,$subjects->grade_id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('grade_id')}}</p>
            </div>

                <label class="col-md-2 control-label">‫ الصف الدراسي   </label>
            <div class="col-md-10">
          {!! Form::select('semester_id',$semesters,$subjects->semester_id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('semester_id')}}</p>
            </div>

             <label class="col-md-2 control-label">‫ الصف الدراسي  </label>
            <div class="col-md-10">
          {!! Form::select('classroom_id',$rooms,$subjects->classroom_id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('classroom_id')}}</p>
            </div>

            <label class="col-md-2 control-label"> الاسم المادة بالعربية </label>
            <div class="col-md-4">
                  {{Form::text('ar_name',$subjects->ar_name,['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>
             <label class="col-md-2 control-label"> الاسم المادة بالانجليزية </label>
            <div class="col-md-4">
                  {{Form::text('en_name',$subjects->en_name,['class'=>"form-control",'placeholder'=>'الاسم بالانجليزية '])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>

            
            <label class="col-md-2 control-label"> صورة المادة  بالعربى</label>
            <div class="col-md-4">
                   {{Form::file('ar_img',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('ar_img')}}</p>
            </div>
             <label class="col-md-2 control-label"> صورة المادة بالانجليزى</label>
            <div class="col-md-4">
                   {{Form::file('en_img',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('en_img')}}</p>
            </div>
     
            
         
            
          </div>
          </div>
          </div>
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
