@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات المادة الدراسية </b>
            </center>
            
<a href="{{URL::to('/')}}/school/subjects/{{$subjects->id}}/edit" class="btn  btn-warning"> تعديل بيانات الفصل الدراسي </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات المادة الدراسي </th>
              <th style="text-align:center;"> ***** بيانات المادة الدراسي *****</th>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$subjects->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$subjects->en_name}}</td>
              </tr>

              <tr>
                <td>اسم المرحلة</td>
                <td>{{$subjects->grade->ar_name}}</td>
              </tr>

              <tr>
                <td>اسم الصف</td>
                <td>{{$subjects->classroom->ar_name}}</td>
              </tr>

              <tr>
                <td>اسم الفصل</td>
                <td>{{$subjects->semester->ar_name}}</td>
              </tr>

               <tr  style="background-color: #ccc;">
                <td>صورة الماده عربى</td>
                <td><img src="{{URL::to('/')}}/{{$subjects->ar_img}}" height="150" width="200"></td>
              </tr>

               <tr  style="background-color: #ccc;">
                <td>صورة المادة انجليزي</td>
                <td><img src="{{URL::to('/')}}/{{$subjects->en_img}}" height="150" width="200"></td>
              </tr>

                 
              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
