@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    المعرض <small>قائمة المعرض</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/admin')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/admin/approved_cards')}}">المواد الدراسية الدراسية</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                 <th> <strong> اسم المادة بالعربية </strong></th>
                 <th> <strong> اسم المادة بالانجليزية </strong></th>
                  <th> <strong> اسم المرحلة</strong></th>
                   <th> <strong> اسم الفصل</strong></th>
                    <th> <strong> اسم الصف</strong></th>
                  
            </tr>
        </thead>
    
        <tbody>
               @foreach($subjects as $subject)

                  <tr>
                    <td class="v-align-middle" ><a href="{{URL::to('/')}}/school/subjects/{{$subject->id}}">
                    {{$subject->ar_name}}</a></td>
                    <td class="v-align-middle" ><a href="{{URL::to('/')}}/school/subjects/{{$subject->id}}">
                    {{$subject->en_name}}</a></td>
                    @if(isset($subject->grade->ar_name))
                    <td class="v-align-middle" >{{$subject->grade->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبطة بالمرحلة</td>
                    @endif

                    @if(isset($subject->semester->ar_name))
                    <td class="v-align-middle" >{{$subject->semester->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبطة بالفصل</td>
                    @endif

                    @if(isset($subject->classroom->ar_name))
                    <td class="v-align-middle" >{{$subject->classroom->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبطة بالصف</td>
                    @endif
                    
                  <td>
                  <a href="{{URL::to('/')}}/school/subjects/{{$subject->id}}/edit" class="btn  btn-warning">تعديل</a>
                  

                  {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\SubjectsController@destroy', $subject->id, 'id'=>'form','class'=>"btn btn-danger"]]) !!}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$subject->ar_name}}؟'))
                                $(this).closest('form').submit();"> حذف</a>
                                {{Form::close()}}
                              </td>
                                  </tr>
                  @endforeach              
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop
@section('jsCode')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop


