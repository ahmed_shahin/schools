@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">الاسابيع</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">تعديل الاسبوع</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> تعديل الاسبوع
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'put','url'=>'school/weeks/'.$weeks->id ,'role'=>"form"]) !!}
             <center><b>بيانات السبوع</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">
                
            <label class="col-md-2 control-label"> الاسم الاسبوع بالعربية </label>
            <div class="col-md-4">
                  {{Form::text('ar_name',$weeks->ar_name,['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>
             <label class="col-md-2 control-label"> الاسم الاسبوع بالانجليزية </label>
            <div class="col-md-4">
                  {{Form::text('en_name',$weeks->en_name,['class'=>"form-control",'placeholder'=>'الاسم بالانجليزية '])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>

            <label class="col-md-2 control-label">  من </label>
            <div class="col-md-4">
                   {{Form::date('from',$weeks->from,['class'=>"form-control",'placeholder'=>'دوام المرحلة'])}}       
                    <p class='text-danger'>{{$errors->first('from')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫الي  </label>
            <div class="col-md-4">
                   {{Form::date('to',$weeks->to,['class'=>"form-control",'placeholder'=>'دوام المرحلة'])}}       
                    <p class='text-danger'>{{$errors->first('to')}}</p>
            </div>

            

            
            
          </div>
          </div> 
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
