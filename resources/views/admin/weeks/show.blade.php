@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات الاسبوع </b>
            </center>
            
<a href="{{URL::to('/')}}/school/weeks/{{$weeks->id}}/edit" class="btn  btn-warning"> تعديل بيانات الاسبوع </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات الاسبوع </th>
              <th style="text-align:center;"> ***** بيانات الاسبوع *****</th>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$weeks->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$weeks->en_name}}</td>
              </tr>

              <tr>
                <td>من</td>
                <td>{{$weeks->from}}</td>
              </tr>

               <tr>
                <td>الي</td>
                <td>{{$weeks->to}}</td>
              </tr>

            
                 
              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
