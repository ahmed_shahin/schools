@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات المدرسة </b>
            </center>
            
<a href="{{URL::to('/')}}/admin/schools/{{$school->id}}/edit" class="btn  btn-warning"> تعديل بيانات المدرسة </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات المدرسة </th>
              <th style="text-align:center;"> ***** بيانات المدرسة *****</th>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$school->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$school->en_name}}</td>
              </tr>

              <tr>
                <td>الشركة</td>
                <td>{{$school->company->ar_name}}</td>
              </tr>

               <tr>
                <td>البريد الالكترونى </td>
                <td>{{$school->email}}</td>
              </tr>

               <tr>
                <td>‫المدينة</td>
                <td>{{$school->city}}</td>
              </tr>


               <tr>
                <td>‫الحى</td>
                <td>{{$school->street}}</td>
              </tr>

                 <tr>
                <td>شرح‬ ‫لمقر‬ ‫المدرسة</td>
                <td>{{$school->location_details}}</td>
              </tr>

              <tr>
                <td>الشعار عربى مكتوب</td>
                <td>{{$school->ar_logo_text}}</td>
              </tr>

                <tr>
                <td>الشعار انجليزى مكتوب</td>
                <td>{{$school->en_logo_text}}</td>
                </tr>

                <tr  style="background-color: #ccc;">
                <td>الشعار عربى</td>
                <td><img src="{{URL::to('/')}}/{{$school->ar_logo}}" height="150" width="200"></td>
                </tr>

                <tr  style="background-color: #ccc;">
                <td>الشعار انجليزى</td>
                <td><img src="{{URL::to('/')}}/{{$school->en_logo}}" height="150" width="200"></td>
                </tr>

                <th style="text-align:center;"> بيانات المدير </th>
                <th style="text-align:center;"> *****  بيانات  المدير ***** </th>

                <tr>
                <td>‫الاسم‬</td>
                <td>{{$school->manager_name}}</td>
                </tr>

                <tr>
                <td>رقم‬ ‫‫‫الجوال‬ </td>
                <td>{{$school->manager_phone}}</td>
                </tr>

                <tr>
                <td>البريد الالكترونى</td>
                <td>{{$school->manager_email}}</td>
                </tr>

                <tr>
                <td>الفاكس</td>
                <td>{{$school->manager_fax}}</td>
                </tr>

                <th style="text-align:center;"> بيانات سكرتير المدرسة </th>
                <th style="text-align:center;"> *****  بيانات  سكرتير المدرسة  ***** </th>

                <tr>
                <td>‫الاسم‬</td>
                <td>{{$school->secretary_name}}</td>
                </tr>

                <tr>
                <td>رقم‬ ‫‫‫الجوال‬ </td>
                <td>{{$school->secretary_phone}}</td>
                </tr>

                <tr>
                <td>البريد الالكترونى</td>
                <td>{{$school->secretary_email}}</td>
                </tr>

                <tr>
                <td>الفاكس</td>
                <td>{{$school->secretary_fax}}</td>
                </tr>


              <th style="text-align:center;"> بيانات وكيل المدرسة </th>
              <th style="text-align:center;"> *****  بيانات  وكيل المدرسة  ***** </th>

               <tr>
                <td>‫الاسم‬</td>
                <td>{{$school->secretary_name}}</td>
              </tr>

               <tr>
                <td>رقم‬ ‫‫‫الجوال‬ </td>
                <td>{{$school->secretary_phone}}</td>
              </tr>

               <tr>
                <td>البريد الالكترونى</td>
                <td>{{$school->secretary_email}}</td>
              </tr>

                <tr>
                <td>الفاكس</td>
                <td>{{$school->secretary_fax}}</td>
              </tr>


              <th style="text-align:center;"> بيانات الادارة المالية </th>
              <th style="text-align:center;"> ***** بيانات الادارة المالية ***** </th>


               <tr>
                <td>‫ الاسم‬ </td>
                <td>{{$school->officer_name}}</td>
              </tr>

               <tr>
                <td> رقم‬ ‫‫‫الجوال‬ </td>
                <td>{{$school->officer_phone}}</td>
              </tr>

               <tr>
                <td> البريد الالكترونى </td>
                <td>{{$school->officer_email}}</td>
              </tr>

                <tr>
                <td> الفاكس </td>
                <td>{{$school->officer_fax}}</td>
              </tr>

              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
