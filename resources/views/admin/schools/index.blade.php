@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    المعرض <small>قائمة المعرض</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/admin')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/admin/approved_cards')}}">المدارس</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                 <th> <strong> اسم المدرسة بالعربية </strong></th>
                    <th ><strong> الشركة </strong></th>
                    <th> <strong> الدولة</strong></th>
                    <th> <strong> المدينة </strong></th>
                     <th> <strong> البريد الالكترونى </strong></th>
                    <th ><strong> الاعدادات </strong></th>
            </tr>
        </thead>
    
        <tbody>
               @foreach($schools as $school)

                  <tr>
                    <td class="v-align-middle" ><a href="{{URL::to('/')}}/admin/schools/{{$school->id}}">
                    {{$school->ar_name}}</a></td>
                    <td class="v-align-middle" >{{$school->company->ar_name}}</td>
                   <td class="v-align-middle" >{{$school->country}}</td>
                   <td class="v-align-middle" >{{$school->city}}</td>
                   <td class="v-align-middle" >{{$school->email}}</td>
                  <td>
                  <a href="{{URL::to('/')}}/admin/schools/{{$school->id}}/edit" class="btn  btn-warning">تعديل</a>
                  

                  {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\SchoolsController@destroy', $school->id], 'id'=>'form','class'=>"btn btn-danger"]) !!}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$school->name}}؟'))
                                $(this).closest('form').submit();"> حذف</a>
                                {{Form::close()}}
                              </td>
                                  </tr>
                  @endforeach              
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop
@section('jsCode')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop



