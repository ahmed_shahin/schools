@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">الشركات</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة مدرسة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة مدرسة
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'admin/schools' ,'role'=>"form",'files'=>'true']) !!}
             <center><b>بيانات المدرسة</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">
                <label class="col-md-2 control-label">‫ الشركة   </label>
            <div class="col-md-10">
  {!! Form::select('company_id',$companies,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('company_id')}}</p>
            </div>

            <label class="col-md-2 control-label"> الاسم بالعربية </label>
            <div class="col-md-4">
                  {{Form::text('ar_name','',['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>
             <label class="col-md-2 control-label"> الاسم بالانجليزية </label>
            <div class="col-md-4">
                  {{Form::text('en_name','',['class'=>"form-control",'placeholder'=>'الاسم بالانجليزية '])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>
     
            <label class="col-md-2 control-label">الدولة </label>
            <div class="col-md-4">
                   {{Form::text('country','',['class'=>"form-control",'placeholder'=>'الدولة'])}}       
                    <p class='text-danger'>{{$errors->first('country')}}</p>
            </div>
            <label class="col-md-2 control-label"> ‫المدينة </label>
            <div class="col-md-4">
                   {{Form::text('city','',['class'=>"form-control",'placeholder'=>' ‫‫المدينة'])}}       
                    <p class='text-danger'>{{$errors->first('city')}}</p>
            </div>
             <label class="col-md-2 control-label"> ‫الحى </label>
            <div class="col-md-10">
                   {{Form::text('street','',['class'=>"form-control",'placeholder'=>'‫الحى'])}}       
                    <p class='text-danger'>{{$errors->first('street')}}</p>
            </div>
         
            <label class="col-md-2 control-label"> ‫شرح‬ ‫لمقر‬ ‫الشركة‬</label>
            <div class="col-md-10">
                   {{Form::text('location_details','',['class'=>"form-control",'placeholder'=>'‫شرح‬ ‫لمقر‬ ‫الشركة‬'])}}       
                    <p class='text-danger'>{{$errors->first('location_details')}}</p>
            </div>
              <label class="col-md-2 control-label"> الشعار عربى</label>
            <div class="col-md-4">
                   {{Form::file('ar_logo',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('ar_logo')}}</p>
            </div>
             <label class="col-md-2 control-label"> الشعار انجليزى</label>
            <div class="col-md-4">
                   {{Form::file('en_logo',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('en_logo')}}</p>
            </div>
            
              <label class="col-md-2 control-label">  نص الشعار عربى </label>
            <div class="col-md-4">
                   {{Form::text('ar_logo_text','',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('ar_logo_text')}}</p>
            </div>
             <label class="col-md-2 control-label"> نص الشعار انجليزى</label>
            <div class="col-md-4">
                   {{Form::text('en_logo_text','',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('en_logo_text')}}</p>
            </div>
        
          <label class="col-md-2 control-label">‫ مراحل‬ ‫‫المد‬رسة‬   </label>
            <div class="col-md-4">
  {!! Form::select('stages', array(
  ""=>'‫‫اختر مرحلة',
  'nursery'=>'‫حضانة‬ و ‫ر‬وضه‬',
  'preliminary'=>'تمهيدي‬',
  'elementary_school'=>'‫‫مدرسه‬ ابتدائي‬ ',
  'middle_school'=>'‫‫مدرسه‬ متوسطة‬',
  'secondary_school'=>'‫‫مدرسه‬ ثانويه‬',
  'middle_elementary_school'=>'‫‫مدرسه‬ ابتدائي‬ و متوسطة‬',
  'middle_secondary_school'=>'‫‫مدرسه‬ متوسطة‬ و ثانوي',
  'all_srages'=>'‫‫مدرسه‬ لجميع المراحل',
  'special_needs_school'=>'‫‫مدرسه‬ احتياجات خاصة'),
  null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('stages')}}</p>
            </div>

  <label class="col-md-2 control-label">‫ تخصص ‫‫المد‬رسة‬   </label>
  <div class="col-md-4">
  {!! Form::select('specialization', array(
  ""=>'‫‫اختر تخصص ',
  'civil '=>'‫اهلية',
  'international'=>'دولية',
  'governmental'=>'‫‫حكومية ',
  'special_needs_school'=>'‫‫احتياجات خاصة',
  'quran_memorization'=>'‫‫‫‫تحفيظ‬ قرأن‬ '),
  null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('specialization')}}</p>
            </div>

 <label class="col-md-2 control-label">‫نوع المنهج الدراسى </label>
 <div class="col-md-4">
  {!! Form::select('curriculum_type', array(
  ""=>'‫‫اختر نوع المنهج',
  'american'=>'‫‫‬ ‫المنهج الامريكي',
  'british'=>'‫‬ ‫المنهج ‫البريطاني‬',
  'saudi'=>'‫‫‫‬ ‫المنهج ‫السعودي‬ ',
  'egyptian'=>'‫‫‫‬ ‫المنهج ‫المصري‬',
  'indian'=>'‫‬ ‫المنهج ‫الهندي‬',
  'pakistan'=>'‫‫‫المنهج الباكستاني‬',
  'philippines'=>'‫‫‫‫‫المنهج الفليبيني‬',
  'no'=>'‫‫‫لا منهجيه‬ ‫'),
  null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('curriculum_type')}}</p>
            </div>

  <label class="col-md-2 control-label">‫ نوع الطلبة   </label>
  <div class="col-md-4">
  {!! Form::select('stages', array(
  ""=>'‫‫اختر نوع',
  'boys'=>'‫‫اولاد',
  'girls'=>'‫‫بنات',
  'boys_girls'=>'‫‫‫‫اولاد و ‫‫بنات'),
  null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('stages')}}</p>
            </div>
          </div>
          </div>
          </div>

          <div class="form-body">
          <center><b> بيانات مدير المدرسة</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫الاسم‬  </label>
            <div class="col-md-4">
                   {{Form::text('manager_name','',['class'=>"form-control",'placeholder'=>' ‫اسم‬ ‫المالك‬'])}}       
                    <p class='text-danger'>{{$errors->first('manager_name')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫رقم‬ ‫‫‫الجوال‬  </label>
            <div class="col-md-4">
                   {{Form::text('manager_phone','',['class'=>"form-control",'placeholder'=>'رقم‬ ‫‫‫جوال‬ المالك‬'])}}       
                    <p class='text-danger'>{{$errors->first('manager_phone')}}</p>
            </div>
          </div>
          </div>
          </div>

        
          
          <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫البريد الالكترونى  </label>
            <div class="col-md-4">
                   {{Form::text('manager_email','',['class'=>"form-control",'placeholder'=>'البريد الالكترونى للمالك‬'])}}       
                    <p class='text-danger'>{{$errors->first('manager_email')}}</p>
            </div>
             <label class="col-md-2 control-label"> ‫ الفاكس  </label>
            <div class="col-md-4">
                   {{Form::text('manager_fax','',['class'=>"form-control",'placeholder'=>' ‫فاك س المالك‬ '])}}       
                    <p class='text-danger'>{{$errors->first('manager_fax')}}</p>
            </div>
          </div>
          </div>
          </div>

           <div class="form-body">
          <center><b> بيانات وكيل المدرسة</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫الاسم‬  </label>
            <div class="col-md-4">
                   {{Form::text('agent_name','',['class'=>"form-control",'placeholder'=>' ‫اسم‬ ‫الوكيل'])}}       
                    <p class='text-danger'>{{$errors->first('agent_name')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫رقم‬ ‫‫‫الجوال‬  </label>
            <div class="col-md-4">
                   {{Form::text('agent_phone','',['class'=>"form-control",'placeholder'=>'رقم‬ ‫‫‫جوال‬ الوكيل'])}}       
                    <p class='text-danger'>{{$errors->first('agent_phone')}}</p>
            </div>
          </div>
          </div>
          </div>

        
          
          <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫البريد الالكترونى  </label>
            <div class="col-md-4">
                   {{Form::text('agent_email','',['class'=>"form-control",'placeholder'=>'البريد الالكترونى للوكيل'])}}       
                    <p class='text-danger'>{{$errors->first('agent_email')}}</p>
            </div>
             <label class="col-md-2 control-label"> ‫ الفاكس  </label>
            <div class="col-md-4">
                   {{Form::text('agent_fax','',['class'=>"form-control",'placeholder'=>' ‫فاك س الوكيل '])}}       
                    <p class='text-danger'>{{$errors->first('agent_fax')}}</p>
            </div>
          </div>
          </div>
          </div>


             <div class="form-body">
          <center><b> بيانات سكرتير المدرسة</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫الاسم‬  </label>
            <div class="col-md-4">
                   {{Form::text('secretary_name','',['class'=>"form-control",'placeholder'=>' ‫اسم‬ ‫السكرتير'])}}       
                    <p class='text-danger'>{{$errors->first('secretary_name')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫رقم‬ ‫‫‫الجوال‬  </label>
            <div class="col-md-4">
                   {{Form::text('secretary_phone','',['class'=>"form-control",'placeholder'=>'رقم‬ ‫‫‫جوال‬ السكرتير'])}}       
                    <p class='text-danger'>{{$errors->first('secretary_phone')}}</p>
            </div>
          </div>
          </div>
          </div>

        
          
          <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫البريد الالكترونى  </label>
            <div class="col-md-4">
                   {{Form::text('secretary_email','',['class'=>"form-control",'placeholder'=>'البريد الالكترونى للسكرتير'])}}       
                    <p class='text-danger'>{{$errors->first('secretary_email')}}</p>
            </div>
             <label class="col-md-2 control-label"> ‫ الفاكس  </label>
            <div class="col-md-4">
                   {{Form::text('secretary_fax','',['class'=>"form-control",'placeholder'=>' ‫فاك س السكرتير '])}}       
                    <p class='text-danger'>{{$errors->first('secretary_fax')}}</p>
            </div>
          </div>
          </div>
          </div>


            <div class="form-body">
            <center><b>بيانات الادارة المالية</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label">‫ الاسم  </label>
            <div class="col-md-4">
                   {{Form::text('officer_name','',['class'=>"form-control",'placeholder'=>' ‫اسم‬ مسئول‬  الادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_name')}}</p>
            </div>
            <label class="col-md-2 control-label">‫ رقم‬ ‫‫‫جوال‬ </label>
            <div class="col-md-4">
                   {{Form::text('officer_phone','',['class'=>"form-control",'placeholder'=>'رقم‬ ‫‫‫جوال‬ الادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_phone')}}</p>
            </div>

          </div>
          </div>
          </div>

      
          <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label">‫  ‫البريد الالكترونى </label>
            <div class="col-md-4">
                   {{Form::text('officer_email','',['class'=>"form-control",'placeholder'=>' ‫البريد الالكترونى للادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_email')}}</p>
            </div>
            <label class="col-md-2 control-label"> ‫الفاكس الادارة المالية </label>
            <div class="col-md-4">
                   {{Form::text('officer_fax','',['class'=>"form-control",'placeholder'=>' ‫فاكس الادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_fax')}}</p>
            </div>

          </div>
          </div>
          </div>

       
       
             <div class="form-body">
             <center>‫<b>بيانات الدخول</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label">  البريد الالكترونى </label>
            <div class="col-md-10">
                   {{Form::text('email','',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('email')}}</p>
            </div>
              <label class="col-md-2 control-label"> كلمة المرور </label>
            <div class="col-md-4">
                   {{Form::text('password','',['class'=>"form-control",'placeholder'=>' كلمة المرور '])}}         
                    <p class='text-danger'>{{$errors->first('password')}}</p>
            </div>

              <label class="col-md-2 control-label"> تاكيد  كلمة المرور </label>
            <div class="col-md-4">
                   {{Form::text('password_confirmation','',['class'=>"form-control",'placeholder'=>'تاكيد  كلمة المرور '])}}         
          </div>
          </div>
          </div> 
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
