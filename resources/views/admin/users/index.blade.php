@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    المعرض <small>قائمة المعرض</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/admin')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/admin/approved_cards')}}">رسائل التواصل</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                 <th> <strong> الاسم بالانجليزية </strong></th>
                 <th> <strong> الاسم بالعربية </strong></th>
                    <th ><strong> البريد الالكترونى </strong></th>
                    <th> <strong> الجوال </strong></th>
                    <th> <strong> العنوان </strong></th>
                    <th ><strong> الاعدادات </strong></th>
            </tr>
        </thead>
    
        <tbody>
               @foreach($users as $user)

                  <tr>
                    <td class="v-align-middle" >{{$user->ar_name}}</td>
                    <td class="v-align-middle" >{{$user->en_name}}</td>
                   <td class="v-align-middle" >{{$user->email}}</td>
                   <td class="v-align-middle" >{{$user->phone}}</td>
                   <td class="v-align-middle" >{{$user->address}}</td>
                  <td>
                  <a href="{{URL::to('/')}}/admin/users/{{$user->id}}/edit" class="btn  btn-warning">تعديل</a>

                   <a href="{{URL::to('/')}}/admin/images/{{$user->id}}" class="btn  btn-primary">الصور</a>
                   <a href="{{URL::to('/')}}/admin/add_image/{{$user->id}}" class="btn  btn-success">اضافة صورة</a>
                  {{Form::open(['route'=>['admin.users.destroy' , $user->id] ,'method'=>'delete' , 'id'=>'form','class'=>"btn btn-danger"])}}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$user->name}}؟'))
                                $(this).closest('form').submit();"> حذف</a>
                                {{Form::close()}}
                                  </tr>
                  @endforeach              
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop
@section('jsCode')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop


