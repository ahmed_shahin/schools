@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">المتاجر</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة متجر</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة متجر
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body form">
      
    <form class="form-horizontal" method="POST" action="{{URL::to('/')}}/admin/companies" enctype="multipart/form-data">
      @if(Session::has('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
      @endif
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> الاسم بالعربية </label>
            <div class="col-md-9">
                  {{Form::text('ar_name','',['class'=>"form-control",'placeholder'=>'Enter name'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>
          </div>
          </div>

           <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> الاسم بالانجليزية </label>
            <div class="col-md-9">
                  {{Form::text('en_name','',['class'=>"form-control",'placeholder'=>'Enter name'])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>
          </div>
          </div>

          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> القسم </label>
            <div class="col-md-9">
                      {!! Form::select('section_id', $sections,null,['class'=>"form-control"]) !!}
                    <p class='text-danger'>{{$errors->first('section_id')}}</p>
            </div>
          </div>
          </div>

          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> المدينة </label>
            <div class="col-md-9">
            {!! Form::select('city_id', $cities,null,['class'=>"form-control"]) !!}      
          <p class='text-danger'>{{$errors->first('city_id')}}</p>
            </div>
          </div>
          </div>

          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> البريد الالكترونى </label>
            <div class="col-md-9">
                   {{Form::text('email','',['class'=>"form-control",'placeholder'=>'Enter email'])}}       
                    <p class='text-danger'>{{$errors->first('email')}}</p>
            </div>
          </div>
          </div>

          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> الهاتف </label>
            <div class="col-md-9">
                   {{Form::text('phone','',['class'=>"form-control",'placeholder'=>'Enter phone'])}}       
                    <p class='text-danger'>{{$errors->first('phone')}}</p>
            </div>
          </div>
          </div>

          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> كلمة المرور </label>
            <div class="col-md-9">
               {{Form::password('password', array('class' => 'form-control')) }}
                <p class='text-danger'>{{$errors->first('password')}}</p>
            </div>
          </div>
          </div>

          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> تاكيد كلمة المرور </label>
            <div class="col-md-9">
                   {{Form::password('password_confirmation', array('class' => 'form-control')) }}
                  <p class='text-danger'>{{$errors->first('password_confirmation')}}</p>
            </div>
          </div>
          </div>
          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> نسبة الخصم </label>
            <div class="col-md-9">
              {{Form::number('discount','',['class'=>"form-control",'placeholder'=>'نسبة الخصم'])}}  <p class='text-danger'>{{$errors->first('discount')}}</p>
            </div>
          </div>
          </div>
         

          <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> نبذة بالعربية </label>
            <div class="col-md-9">
             {{Form::textarea('ar_details','',['class'=>"form-control",'placeholder'=>'نبذة','id'=>"editor1"])}}  <p class='text-danger'>{{$errors->first('ar_details')}}</p>
            </div>
          </div>
          </div>
            <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> نبذة  بالانجليزية</label>
            <div class="col-md-9">
             {{Form::textarea('en_details','',['class'=>"form-control",'placeholder'=>'نبذة','id'=>"editor2"])}}  <p class='text-danger'>{{$errors->first('en_details')}}</p>
            </div>
          </div>
          </div>

 <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> الصورة </label>
            <div class="col-md-9">
            <input class="form-control" type="file" name="photo" >         
    <p class='text-danger'>{{$errors->first('photo')}}</p>
            </div>
          </div>
          </div>
              <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> العنوان </label>
            <div class="col-md-9">
             {{Form::text('address','',['class'=>"form-control",'placeholder'=>'العنوان','id'=>'a'])}}  <p class='text-danger'>{{$errors->first('address')}}</p>
            </div>
          </div>
          </div>
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label"> خط العرض </label>
            <div class="col-md-3">
                  {{Form::text('lat','',['class'=>"form-control",'placeholder'=>'خط العرض','id'=>'lat'])}}  <p class='text-danger'>{{$errors->first('lat')}}</p>
            </div>
              <label class="col-md-2 control-label"> خط الطول </label>
            <div class="col-md-3">
                 {{Form::text('lang','',['class'=>"form-control",'placeholder'=>'خط الطول','id'=>'lang'])}}  <p class='text-danger'>{{$errors->first('lang')}}</p>
            </div>
              <div class="col-md-3">
        <input type="button" style="width: 100%;border-radius: 4px;
     " class="btn btn-primary" onclick="getLocation()" value="الموقع على الخريطة" >
            </div>
          </div>
          </div>
<center>
حرك المؤشر لتحديد المكان على الخريطة<br><br>
  <div id="map" style="width: 800px;height: 400px;"></div>
  </center>
       <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>
        
</form>
    </div>
  </div>
  <!-- END SAMPLE FORM PORTLET-->


<script type="text/javascript">

   function getLocation()
   {
  if (navigator.geolocation)

    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  else
    x.innerHTML = "Geolocation is not supported by this browser.";

}

function successFunction(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;
  getCurrentLocation(lat, lng)
}


function getCurrentLocation(lat , lng) {
  var geocoder;
geocoder = new google.maps.Geocoder();
var latlng = new google.maps.LatLng(lat,lng);
//alert("Else loop" + latlng);
geocoder.geocode({
    'latLng': latlng

}, 
function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
            var address = results[0].formatted_address;

              document.getElementById('address').value  = address;

        } else {
            alert("address not found");
        }
    } else {
                   alert(" not found");

    }
});
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: new google.maps.LatLng(lat, lng),
  });

  var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(lat, lng),
    draggable: true
  });

  document.getElementById('lat').value  = lat;
  document.getElementById('lang').value = lng;

  google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    document.getElementById('lat').value  = evt.latLng.lat();
    document.getElementById('lang').value = evt.latLng.lng();
  });
  google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
  });

  map.setCenter(myMarker.position);
  myMarker.setMap(map);
}

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: new google.maps.LatLng(24.739, 46.700),
  });

  var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(24.739, 46.700),
    draggable: true
  });

  google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    document.getElementById('lat').value  = evt.latLng.lat();
    document.getElementById('lang').value = evt.latLng.lng();
  });
  google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
  });

  map.setCenter(myMarker.position);
  myMarker.setMap(map);
}
function errorFunction() {
  alert("Geocoder failed");
}



</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc-9IIsd_1P8yfhMZe8_D7EYv5O2Vt9-E&language=ar&callback=initMap">

</script>
  
    @stop
