@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b>بيانات االشركة</b>
            </center>
            
<a href="{{URL::to('/')}}/admin/companies/{{$company->id}}/edit" class="btn  btn-warning">تعديل بيانات الشركة</a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;">بيانات االشركة</th>
              <th style="text-align:center;"> ***** بيانات الشركة *****</th>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$company->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$company->en_name}}</td>
              </tr>

              <tr>
                <td>رقم السجل التجارى</td>
                <td>{{$company->commercial_no}}</td>
              </tr>

               <tr>
                <td>الدولة </td>
                <td>{{$company->country}}</td>
              </tr>

               <tr>
                <td>‫المدينة</td>
                <td>{{$company->city}}</td>
              </tr>


               <tr>
                <td>‫الحى</td>
                <td>{{$company->street}}</td>
              </tr>

                 <tr>
                <td>شرح‬ ‫لمقر‬ ‫الشركة‬</td>
                <td>{{$company->location_details}}</td>
              </tr>
             

                <tr  style="background-color: #ccc;">
                <td>الشعار عربى</td>
                <td><img src="{{URL::to('/')}}/{{$company->ar_logo}}" height="150" width="200"></td>
              </tr>

                  <tr  style="background-color: #ccc;">
                <td>الشعار انجليزى</td>
                <td><img src="{{URL::to('/')}}/{{$company->en_logo}}" height="150" width="200"></td>
              </tr>

              <th style="text-align:center;">بيانات المالك </th>
              <th style="text-align:center;"> ***** بيانات المالك  *****</th>

               <tr>
                <td>‫الاسم‬</td>
                <td>{{$company->owner_name}}</td>
              </tr>

               <tr>
                <td>رقم‬ ‫‫‫الجوال‬ </td>
                <td>{{$company->owner_phone}}</td>
              </tr>

               <tr>
                <td>البريد الالكترونى</td>
                <td>{{$company->owner_email}}</td>
              </tr>

                <tr>
                <td>الفاكس</td>
                <td>{{$company->owner_fax}}</td>
              </tr>


              <th style="text-align:center;">بيانات الادارة المالية</th>
              <th style="text-align:center;"> ***** بيانات الادارة المالية *****</th>


               <tr>
                <td>‫الاسم‬</td>
                <td>{{$company->officer_name}}</td>
              </tr>

               <tr>
                <td>رقم‬ ‫‫‫الجوال‬ </td>
                <td>{{$company->officer_phone}}</td>
              </tr>

               <tr>
                <td>البريد الالكترونى</td>
                <td>{{$company->officer_email}}</td>
              </tr>

                <tr>
                <td>الفاكس</td>
                <td>{{$company->officer_fax}}</td>
              </tr>
              <th style="text-align:center;">المرفقات</th>
              <th style="text-align:center;"> ***** المرفقات *****</th>
                <tr>
                <td>‫العقد‬</td>
                <td><a href="{{URL::to('/')}}/{{$company->contract}}" target="_blanck">‫العقد‬</a></td>
                </tr>

                <tr>
                <td>‫السجل‬</td>
                <td><a href="{{URL::to('/')}}/{{$company->commercial_license}}" target="_blanck">‫السجل‬  التجارى</a></td>
              </tr>

                <tr>
                <td>ا‫لمعلومات‬ ‫البنكية‬ ‫للشركة‬</td>
                <td><a href="{{URL::to('/')}}/{{$company->bank_data}}" target="_blanck">ا‫لمعلومات‬ ‫البنكية‬ ‫للشركة‬</a></td>
              </tr>

              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
