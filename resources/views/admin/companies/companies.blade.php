@extends('admin.layouts.dashboard')
 @section('content')
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
          {{--   <a href="{{URL::to('/')}}/users/create" class="btn  btn-primary">اضافة</a> --}}
              <h4> <span class="semi-bold">المستخدمين</span></h4>
              <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
            </div>
            <div class="grid-body ">
              <table class="table table-hover table-condensed" id="example" style="direction: rtl;">
                <thead>
                  <tr>
                   <td></td>
                    <td> <strong> الاسم </strong></td>
                    <td ><strong> قبول / تعطيل </strong></td>
                    <td ><strong> حذف </strong></td>
                  </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)

                  <tr >
                  <td></td>
                    <td class="v-align-middle" >
                    {{$company->ar_name}}
                    </td>
                  <td>
                    @if($company->approved =="0")<a href="{{URL::to('/')}}/admin/approved/{{$company->id}}" class="btn btn-primary">قبول</a>@else<a href="{{URL::to('/')}}/admin/pending/{{$company->id}}" class="btn btn-warning">تعليق</a>@endif
                  </td>
                  <td>

                  {{-- <a href="{{URL::to('/')}}/users/{{$company->id}}/edit" class="btn  btn-warning">تعديل</a> --}}
                  {{Form::open(['route'=>['admin.users.destroy',$company->id] ,'method'=>'delete' , 'id'=>'form','class'=>"btn btn-danger"])}}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('تاكيد الحذف؟'))
                                $(this).closest('form').submit();"> حذف</a>
                                {{Form::close()}}
                                  </tr>
                  @endforeach              
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
        </div>
      @stop