@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">الشركات</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">تعديل شركة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> تعديل شركة
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'put','url'=>'admin/companies/'.$company->id ,'role'=>"form",'files'=>'true']) !!}
             <center><b>بيانات الشركة</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">

            <label class="col-md-2 control-label"> الاسم بالعربية </label>
            <div class="col-md-4">
                  {{Form::text('ar_name',$company->ar_name,['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>
             <label class="col-md-2 control-label"> الاسم بالانجليزية </label>
            <div class="col-md-4">
                  {{Form::text('en_name',$company->en_name,['class'=>"form-control",'placeholder'=>'الاسم بالانجليزية '])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>
     
            <label class="col-md-2 control-label">الدولة </label>
            <div class="col-md-4">
                   {{Form::text('country',$company->country,['class'=>"form-control",'placeholder'=>'الدولة'])}}       
                    <p class='text-danger'>{{$errors->first('country')}}</p>
            </div>
            <label class="col-md-2 control-label"> ‫المدينة </label>
            <div class="col-md-4">
                   {{Form::text('city',$company->city,['class'=>"form-control",'placeholder'=>' ‫‫المدينة'])}}       
                    <p class='text-danger'>{{$errors->first('city')}}</p>
            </div>
             <label class="col-md-2 control-label"> ‫الحى </label>
            <div class="col-md-4">
                   {{Form::text('street',$company->street,['class'=>"form-control",'placeholder'=>'‫الحى'])}}       
                    <p class='text-danger'>{{$errors->first('street')}}</p>
            </div>
         
            <label class="col-md-2 control-label"> رقم السجل التجارى </label>
            <div class="col-md-4">
                   {{Form::text('commercial_no',$company->commercial_no,['class'=>"form-control",'placeholder'=>'رقم السجل التجارى'])}}       
                    <p class='text-danger'>{{$errors->first('commercial_no')}}</p>
            </div>
            <label class="col-md-2 control-label"> ‫شرح‬ ‫لمقر‬ ‫الشركة‬</label>
            <div class="col-md-10">
                   {{Form::text('location_details',$company->location_details,['class'=>"form-control",'placeholder'=>'‫شرح‬ ‫لمقر‬ ‫الشركة‬'])}}       
                    <p class='text-danger'>{{$errors->first('location_details')}}</p>
            </div>
              <label class="col-md-2 control-label"> الشعار عربى</label>
            <div class="col-md-4">
                   {{Form::file('ar_logo',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('ar_logo')}}</p>
            </div>
             <label class="col-md-2 control-label"> الشعار انجليزى</label>
            <div class="col-md-4">
                   {{Form::file('en_logo',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('en_logo')}}</p>
            </div>
        
          </div>
          </div>
          </div>

          <div class="form-body">
          <center><b> بيانات المالك</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫الاسم‬  </label>
            <div class="col-md-4">
                   {{Form::text('owner_name',$company->owner_name,['class'=>"form-control",'placeholder'=>' ‫اسم‬ ‫المالك‬'])}}       
                    <p class='text-danger'>{{$errors->first('owner_name')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫رقم‬ ‫‫‫الجوال‬  </label>
            <div class="col-md-4">
                   {{Form::text('owner_phone',$company->owner_phone,['class'=>"form-control",'placeholder'=>'رقم‬ ‫‫‫جوال‬ المالك‬'])}}       
                    <p class='text-danger'>{{$errors->first('owner_phone')}}</p>
            </div>
          </div>
          </div>
          </div>

        
          
          <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> ‫البريد الالكترونى  </label>
            <div class="col-md-4">
                   {{Form::text('owner_email',$company->owner_email,['class'=>"form-control",'placeholder'=>'البريد الالكترونى للمالك‬'])}}       
                    <p class='text-danger'>{{$errors->first('owner_email')}}</p>
            </div>
             <label class="col-md-2 control-label"> ‫ الفاكس  </label>
            <div class="col-md-4">
                   {{Form::text('owner_fax',$company->owner_fax,['class'=>"form-control",'placeholder'=>' ‫فاك س المالك‬ '])}}       
                    <p class='text-danger'>{{$errors->first('owner_fax')}}</p>
            </div>
          </div>
          </div>
          </div>

            <div class="form-body">
            <center><b>بيانات الادارة المالية</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label">‫ الاسم  </label>
            <div class="col-md-4">
                   {{Form::text('officer_name',$company->officer_name,['class'=>"form-control",'placeholder'=>' ‫اسم‬ مسئول‬  الادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_name')}}</p>
            </div>
            <label class="col-md-2 control-label">‫ رقم‬ ‫‫‫جوال‬ </label>
            <div class="col-md-4">
                   {{Form::text('officer_phone',$company->officer_phone,['class'=>"form-control",'placeholder'=>'رقم‬ ‫‫‫جوال‬ الادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_phone')}}</p>
            </div>

          </div>
          </div>
          </div>

      
          <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label">‫  ‫البريد الالكترونى </label>
            <div class="col-md-4">
                   {{Form::text('officer_email',$company->officer_email,['class'=>"form-control",'placeholder'=>' ‫البريد الالكترونى للادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_email')}}</p>
            </div>
            <label class="col-md-2 control-label"> ‫الفاكس  </label>
            <div class="col-md-4">
                   {{Form::text('officer_fax',$company->officer_fax,['class'=>"form-control",'placeholder'=>' ‫فاكس الادارة المالية'])}}       
                    <p class='text-danger'>{{$errors->first('officer_fax')}}</p>
            </div>

          </div>
          </div>
          </div>

       
       
             <div class="form-body">
             <center>‫<b>المرفقات</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label">  ‫العقد‬ </label>
            <div class="col-md-4">
                   {{Form::file('contract',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('contract')}}</p>
            </div>
              <label class="col-md-2 control-label"> ‫السجل‬ </label>
            <div class="col-md-4">
                   {{Form::file('commercial_license',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('commercial_license')}}</p>
            </div>
          </div>
          </div>
          </div>
   
          <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-3 control-label"> ا‫لمعلومات‬ ‫البنكية‬ ‫للشركة‬ </label>
            <div class="col-md-8">
                   {{Form::file('bank_data',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('bank_data')}}</p>
            </div>
          </div>
          </div>
          </div>  
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
