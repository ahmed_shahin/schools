@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات المرحلة الدراسية </b>
            </center>
            
<a href="{{URL::to('/')}}/school/schoolGrades/{{$grades->id}}/edit" class="btn  btn-warning"> تعديل بيانات المرحلة الدراسية </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات المرحلة الدراسية </th>
              <th style="text-align:center;"> ***** بيانات المرحلة الدراسية *****</th>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$grades->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$grades->en_name}}</td>
              </tr>

              <tr>
                <td>اسم المدرسة</td>
                <td>{{$grades->school->ar_name}}</td>
              </tr>

               <tr>
                <td>اسم المسئول عن المرحلة </td>
                <td>{{$grades->manager}}</td>
              </tr>

               <tr>
                <td>الجوال</td>
                <td>{{$grades->phone}}</td>
              </tr>


               <tr>
                <td>الايميل</td>
                <td>{{$grades->email}}</td>
              </tr>

              <tr>
                <td>المكتب</td>
                <td>{{$grades->office}}</td>
              </tr>

              <tr>
                <td>التحويل</td>
                <td>{{$grades->transfer}}</td>
              </tr>

                 
              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
