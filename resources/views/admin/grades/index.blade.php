@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    المعرض <small>قائمة المعرض</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/admin')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/admin/approved_cards')}}">المراحل الرئيسية</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                 <th> <strong> اسم المرحلة بالعربية </strong></th>
                 <th> <strong> اسم المرحلة بالانجليزية </strong></th>
                    <th> <strong> اسم المدرسة</strong></th>
                    <th ><strong> اسم المسئول عن المرحلة </strong></th>
                    <th> <strong> الجوال</strong></th>
                    <th> <strong> الايميل </strong></th>
                    <th ><strong> المكتب </strong></th>
                    <th ><strong> التحويلة </strong></th>
            </tr>
        </thead>
    
        <tbody>
               @foreach($grades as $grade)

                  <tr>
                    <td class="v-align-middle" ><a href="{{URL::to('/')}}/school/schoolGrades/{{$grade->id}}">
                    {{$grade->ar_name}}</a></td>
                    <td class="v-align-middle" ><a href="{{URL::to('/')}}/school/schoolGrades/{{$grade->id}}">
                    {{$grade->en_name}}</a></td>
                    @if(isset($grade->school->ar_name))
                    <td class="v-align-middle" >{{$grade->school->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبطة بالمدرسة</td>
                    @endif
                    <td class="v-align-middle" >{{$grade->manager}}</td>
                   <td class="v-align-middle" >{{$grade->phone}}</td>
                   <td class="v-align-middle" >{{$grade->email}}</td>
                   <td class="v-align-middle" >{{$grade->office}}</td>
                   <td class="v-align-middle" >{{$grade->transfer}}</td>
                  <td>
                  <a href="{{URL::to('/')}}/school/schoolGrades/{{$grade->id}}/edit" class="btn  btn-warning">تعديل</a>
                  

                  {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\SchoolGradesController@destroy', $grade->id, 'id'=>'form','class'=>"btn btn-danger"]]) !!}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$grade->name}}؟'))
                                $(this).closest('form').submit();"> حذف</a>
                                {{Form::close()}}
                                  </tr>
                  @endforeach              
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop
@section('jsCode')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop


