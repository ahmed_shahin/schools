@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">المراحل الدراسية</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة مرحلة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة مرحلة
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'school/schoolGrades' ,'role'=>"form"]) !!}
             <center><b>بيانات المرحلة</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">
                <label class="col-md-2 control-label">‫ المدرسة   </label>
            <div class="col-md-10">
          {!! Form::select('school_id',$schools,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('school_id')}}</p>
            </div>

            <label class="col-md-2 control-label">  المرحلة بالعربية </label>
            <div class="col-md-4">
                  {{Form::text('ar_name','',['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>
             <label class="col-md-2 control-label">  المرحلة بالانجليزية </label>
            <div class="col-md-4">
                  {{Form::text('en_name','',['class'=>"form-control",'placeholder'=>'الاسم بالانجليزية '])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>
     
            <label class="col-md-2 control-label">اسم المسئول عن المرحلة </label>
            <div class="col-md-4">
                   {{Form::text('manager','',['class'=>"form-control",'placeholder'=>'المسئول عن المرحلة'])}}       
                    <p class='text-danger'>{{$errors->first('manager')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫رقم‬ ‫‫‫الجوال‬  </label>
            <div class="col-md-4">

                   {{Form::text('phone','',['class'=>"form-control",'placeholder'=>'رقم‬ ‫‫‫الجوال‬'])}}       

                    <p class='text-danger'>{{$errors->first('phone')}}</p>
            </div>
            <label class="col-md-2 control-label"> ‫البريد الالكترونى  </label>
            <div class="col-md-4">

                   {{Form::text('email','',['class'=>"form-control",'placeholder'=>'البريد الالكترونى'])}}

                    <p class='text-danger'>{{$errors->first('email')}}</p>
            </div>
            <label class="col-md-2 control-label"> المكتب </label>
            <div class="col-md-4">
                   {{Form::text('office','',['class'=>"form-control",'placeholder'=>' المكتب'])}}       
                    <p class='text-danger'>{{$errors->first('office')}}</p>
            </div>
             <label class="col-md-2 control-label"> التحويلة </label>
            <div class="col-md-4">
                   {{Form::text('transfer','',['class'=>"form-control",'placeholder'=>'التحويلة'])}}       
                    <p class='text-danger'>{{$errors->first('transfer')}}</p>
            </div>

            
             <label class="col-md-2 control-label">  ‫عدد الفسح  </label>
            <div class="col-md-4">
                   {{Form::text('break','',['class'=>"form-control",'placeholder'=>' ‫عدد الفسح'])}}       
                    <p class='text-danger'>{{$errors->first('break')}}</p>
            </div>
           

            <label class="col-md-2 control-label">  ‫دوام المرحلة &ensp;&ensp; من </label>
            <div class="col-md-4">
                   {{Form::time('from','',['class'=>"form-control",'placeholder'=>'دوام المرحلة'])}}       
                    <p class='text-danger'>{{$errors->first('from')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫الي  </label>
            <div class="col-md-4">
                   {{Form::time('to','',['class'=>"form-control",'placeholder'=>'دوام المرحلة'])}}       
                    <p class='text-danger'>{{$errors->first('to')}}</p>
            </div>

            

             <label class="col-md-2 control-label">  ‫عدد الحصص  </label>
            <div class="col-md-10">
                   {{Form::text('lesons','',['class'=>"form-control",'placeholder'=>' ‫عدد الحصص '])}}       
                    <p class='text-danger'>{{$errors->first('lesons')}}</p>
            </div>
          
          <label class="col-md-2 control-label">  ‫عدد ايام التدريس  </label>
          
          @foreach($dayes as $key => $day)
            <div class="col-md-2">
            {{$day}}
            {{ Form::checkbox('workday[]', $day) }}
          </div>
          @endforeach
            
          </div>
          </div> 
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
