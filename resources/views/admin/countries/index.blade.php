@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/admin')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/admin/country')}}">المناطق</a>
            <i class="fa fa-angle-right"></i>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th> اسم المنطقة</th>
                <th>تعديل</th>
                <th>حذف</th>
            </tr>
        </thead>
    
        <tbody>
       @foreach($countries as $country)
               <tr>
                  <td>{{$country->name}}</td>
                  <td><a href="{{URL::to('/')}}/admin/country/{{$country->id}}/edit" class="btn  btn-primary"><i class="fa fa-pencil-square-o btn  btn-primary "></i>تعديل</a></td>
                  <td>
                   {{Form::open(['method'=>'DELETE', 'action'=>['Admin\CountriesController@destroy', $country->id], 'id'=>'form','class'=>"btn btn-danger"]) }}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$country->name}}؟'))
                                $(this).closest('form').submit();"><i class="fa fa-trash-o btn  btn-danger "></i> حذف</a>
                                {{Form::close()}}
                 </td>
                </tr>
                @endforeach
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop


