@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">الفصول الدراسية</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة مرحلة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة فصل دراسي
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'school/semesters' ,'role'=>"form"]) !!}
             <center><b>بيانات الفصل</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">
                <label class="col-md-2 control-label">‫ الصف   </label>
            <div class="col-md-10">
          {!! Form::select('classroom_id',$rooms,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('classroom_id')}}</p>
            </div>

            <label class="col-md-2 control-label"> الاسم الفصل بالعربية </label>
            <div class="col-md-4">
                  {{Form::text('ar_name','',['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>
             <label class="col-md-2 control-label"> الاسم الفصل بالانجليزية </label>
            <div class="col-md-4">
                  {{Form::text('en_name','',['class'=>"form-control",'placeholder'=>'الاسم بالانجليزية '])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>
     
           
         
            
          </div>
          </div> 
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
