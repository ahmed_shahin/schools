@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات الفصل الدراسي </b>
            </center>
            
<a href="{{URL::to('/')}}/school/semesters/{{$semesters->id}}/edit" class="btn  btn-warning"> تعديل بيانات الفصل الدراسي </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات الفصل الدراسي </th>
              <th style="text-align:center;"> ***** بيانات الفصل الدراسي *****</th>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$semesters->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$semesters->en_name}}</td>
              </tr>

              <tr>
                <td>اسم الصف</td>
                <td>{{$semesters->classroom->ar_name}}</td>
              </tr>

              

                 
              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
