@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('school')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('schedules')}}">جدول الحصص</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">تعديل المادة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> تعديل جدول الحصص
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'put','url'=>'school/tests/'.$tests->id ,'role'=>"form"]) !!}
             <center><b>بيانات الاختبار </b></center><br>
              
        <div class="form-body">
          <div class="form-group">
            <div class="row">

              <label class="col-md-2 control-label">الاسم</label>
            <div class="col-md-4">
                  {{Form::text('name',$tests->name,['class'=>"form-control",'placeholder'=>'الاسم الاسم'])}}       
                    <p class='text-danger'>{{$errors->first('name')}}</p>
            </div>
                      
            <label class="col-md-2 control-label">التاريخ</label>
            <div class="col-md-4">
                  {{Form::date('date',$tests->date,['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('date')}}</p>
            </div>

            
             <label class="col-md-2 control-label">‫ المسمي المرحلة </label>
            <div class="col-md-4">
          {!! Form::select('grade_id',$grades,$tests->grade_id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('grade_id')}}</p>
            </div>

            <label class="col-md-2 control-label"> ‫الصف </label>
            <div class="col-md-4">
          {!! Form::select('room_id',$rooms,$tests->room_id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('room_id')}}</p>
            </div>

             <label class="col-md-2 control-label">‫ الفصل </label>
            <div class="col-md-4">
          {!! Form::select('semester_id',$semesters,$tests->semester_id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('semester_id')}}</p>
            </div>

            <label class="col-md-2 control-label">‫اليوم </label>
            <div class="col-md-4">
          {!! Form::select('day_id',$dayes,$tests->day_id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('day_id')}}</p>
            </div>

            
          </div>
          </div>
          </div>
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
