@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات الصف الدراسي </b>
            </center>
            
<a href="{{URL::to('/')}}/school/classrooms/{{$classrooms->id}}/edit" class="btn  btn-warning"> تعديل بيانات الصف الدراسي </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات الصف الدراسي </th>
              <th style="text-align:center;"> ***** بيانات الصف الدراسي *****</th>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$classrooms->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$classrooms->en_name}}</td>
              </tr>

              <tr>
                <td>اسم المرحلة</td>
                <td>{{$classrooms->grade->ar_name}}</td>
              </tr>

              

                 
              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
