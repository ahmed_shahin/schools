@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('school')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('results')}}">الجدول</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة جدول</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة نتيجة
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'school/testresults' ,'role'=>"form"]) !!}
             <center><b>نتيجة الاختبار</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
            

             @foreach($students as $key => $student)
          <div class="row">
            
                <input type="hidden" name="count" value="{{$count}}">
                <input type="hidden" name="resultid" value="{{$resultID}}">
                
            
            <label class="col-md-2 control-label">‫ <strong> اسم الطالب </strong>  {{$student->name}} </label>
            <div class="col-md-4">
          {!! Form::hidden('student_id',$student->id,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('student_id')}}</p>
            </div>
            
             <label class="col-md-2 control-label"> النتيجه </label>
            <div class="col-md-4">
                  {{Form::text('result'.$key,'',['class'=>"form-control",'placeholder'=>'النتيجه '])}}       
                    <p class='text-danger'>{{$errors->first('result')}}</p>
            </div>

             
         
            
          </div>
          <hr>
          @endforeach

          </div> 

           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
