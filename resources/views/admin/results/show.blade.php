@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات هيئة الموظفين </b>
            </center>
            
<a href="{{URL::to('/')}}/school/staffs/{{$staffs->id}}/edit" class="btn  btn-warning"> تعديل بيانات هيئة الموظفين </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات هيئة الموظفين </th>
              <th style="text-align:center;"> ***** بيانات هيئة الموظفين *****</th>

              <tr>
                <td>اسم المستخدم</td>
                <td>{{$staffs->username}}</td>
              </tr>

              <tr>
                <td>الاسم بالعربية</td>
                <td>{{$staffs->ar_name}}</td>
              </tr>

               <tr>
                <td>الاسم بالانجليزية</td>
                <td>{{$staffs->en_name}}</td>
              </tr>

              <tr>
                <td>اسم المسمي الوظيفي</td>
                <td>{{$staffs->jobtitle->ar_name}}</td>
              </tr>

              <tr>
                <td>الجوال</td>
                <td>{{$staffs->phone}}</td>
              </tr>

              <tr>
                <td>المكان</td>
                <td>{{$staffs->place}}</td>
              </tr>

              <tr>
                <td>رقم البطاقة</td>
                <td>{{$staffs->card}}</td>
              </tr>

              <tr>
                <td>البريد الالكتروني</td>
                <td>{{$staffs->email}}</td>
              </tr>

              <tr>
                <td>النوع</td>
                @if($staffs->type == 0)
                <td>ذكر</td>
                @else
                <td>انثي</td>
                @endif
              </tr>

              <tr>
                <td>اسم الماده</td>
                <td>{{$staffs->subject->ar_name}}</td>
              </tr>

              <tr>
                <td>الجنسية</td>
                <td>{{$staffs->nationality->ar_name}}</td>
              </tr>

              <tr>
                <td>اللغة الام</td>
                <td>{{$staffs->language->ar_name}}</td>
              </tr>

              <tr>
                <td>الدولة</td>
                <td>{{$staffs->country->name}}</td>
              </tr>

               <tr>
                <td>المدينة</td>
                <td>{{$staffs->city->name}}</td>
              </tr>
                 
              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
