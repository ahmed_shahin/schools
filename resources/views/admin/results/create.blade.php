@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('school')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('results')}}">الجدول</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة جدول</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة نتيجة
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'school/results' ,'role'=>"form"]) !!}
             <center><b>نتيجة الاختبار</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">
            
            <label class="col-md-2 control-label">‫اسم الاختبار </label>
            <div class="col-md-4">
          {!! Form::select('test_id',$tests,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('test_id')}}</p>
            </div>
            
             <label class="col-md-2 control-label">‫ المرحلة </label>
            <div class="col-md-4">
          {!! Form::select('grade_id',$grades,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('grade_id')}}</p>
            </div>

            <label class="col-md-2 control-label"> ‫الصف </label>
            <div class="col-md-4">
          {!! Form::select('room_id',$rooms,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('room_id')}}</p>
            </div>

             <label class="col-md-2 control-label">‫ الفصل </label>
            <div class="col-md-4">
          {!! Form::select('semester_id',$semesters,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('semester_id')}}</p>
            </div>


            

          

                     
         
            
          </div>
          </div> 
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
