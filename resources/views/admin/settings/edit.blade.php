@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('school')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('school/albums')}}">الابومات</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة البوم</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif


   @if(Session::has('edit_message'))

<div id="edit_message" class="alert alert-success">
  <strong>
                    {{Session::get('edit_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة البوم
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body form">
      {!! Form::open(['method'=>'put','url'=>'school/settings/update/'.$settings->id,'class'=>"form-horizontal" ,'role'=>"form"]) !!}
      <br>
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-1 control-label">عن المدرسة  </label>
            <div class="col-md-10">
                    {{Form::textarea('about_us',$settings->about_us,['class'=>"form-control"])}}
                    <p class='text-danger'>{{$errors->first('about_us')}}</p>
            </div>
          </div>

        
          <div class="form-group">
            <label class="col-md-1 control-label"> الاهداف </label>
            <div class="col-md-10">
                    {{Form::textarea('goals',$settings->goals,['class'=>"form-control"])}}
                    <p class='text-danger'>{{$errors->first('goals')}}</p>
            </div>
          </div>
         

        
          <div class="form-group">
            <label class="col-md-1 control-label"> كلمة مدير المدرسة </label>
            <div class="col-md-10">
                    {{Form::textarea('manager_letter',$settings->manager_letter,['class'=>"form-control"])}}
                    <p class='text-danger'>{{$errors->first('manager_letter')}}</p>
            </div>
          </div>
        

          
          <div class="form-group">
            <label class="col-md-1 control-label"> رسالتنا </label>
            <div class="col-md-10">
                    {{Form::textarea('mission',$settings->mission,['class'=>"form-control"])}}
                    <p class='text-danger'>{{$errors->first('mission')}}</p>
            </div>
          </div>
          </div>
          
        <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
  
  
  
    @stop

 