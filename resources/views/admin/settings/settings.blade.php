@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
    
      <li>
        <a href="#">اعدادات الموقع</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اعدادات الموقع
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body form">
      {!! Form::open(['method'=>'PUT','url'=>'admin/settings/'.$settings->id,'class'=>"form-horizontal" ,'role'=>"form"]) !!}
      @if(Session::has('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
      @endif
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-1 control-label"> الجوال </label>
            <div class="col-md-10">
                   <input class="form-control" type="text" name="phone1" placeholder="اسم المدينة" value="{{$settings->phone1}}">         
                    <p class='text-danger'>{{$errors->first('phone1')}}</p>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-1 control-label"> الفيس بوك </label>
            <div class="col-md-10">
                   <input class="form-control" type="text" name="facebook" placeholder="اسم المدينة" value="{{$settings->facebook}}">         
                    <p class='text-danger'>{{$errors->first('facebook')}}</p>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-1 control-label"> جوجل </label>
            <div class="col-md-10">
                   <input class="form-control" type="text" name="google" placeholder="اسم المدينة" value="{{$settings->google}}">         
                    <p class='text-danger'>{{$errors->first('google')}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label"> يوتيوب </label>
            <div class="col-md-10">
                   <input class="form-control" type="text" name="youtube" placeholder="اسم المدينة" value="{{$settings->youtube}}">         
                    <p class='text-danger'>{{$errors->first('youtube')}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label"> سناب </label>
            <div class="col-md-10">
                   <input class="form-control" type="text" name="snap" placeholder="اسم المدينة" value="{{$settings->snap}}">         
                    <p class='text-danger'>{{$errors->first('snap')}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label"> انستجرام </label>
            <div class="col-md-10">
                   <input class="form-control" type="text" name="instagram" placeholder="اسم المدينة" value="{{$settings->instagram}}">         
                    <p class='text-danger'>{{$errors->first('instagram')}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label"> الشعار </label>
            <div class="col-md-10">
                   <input class="form-control" type="file" name="logo">         
                    <p class='text-danger'>{{$errors->first('logo')}}</p>
            </div>
          </div>
        <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
  <!-- END SAMPLE FORM PORTLET-->
  @stop

