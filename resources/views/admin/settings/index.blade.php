@extends('admin.layouts.dashboard')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b>الاعدادات</b>
            </center>
            
<a href="{{URL::to('/')}}/admin/settings/{{$settings->id}}/edit" class="btn  btn-warning">edit</a>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">
              <tr>
                <td>description</td>
                <td>{{$settings->description}}</td>
              </tr>

               <tr>
                <td>keywords</td>
                <td>{{$settings->keywords}}</td>
              </tr>

               <tr>
                <td>phone1 </td>
                <td>{{$settings->phone1}}</td>
              </tr>

               <tr>
                <td>phone2</td>
                <td>{{$settings->phone2}}</td>
              </tr>


               <tr>
                <td>plane</td>
                <td>{{$settings->plane}}</td>
              </tr>

                <tr>
                <td>linkedin</td>
                <td>{{$settings->linkedin}}</td>
              </tr>

                 <tr>
                <td>skype</td>
                <td>{{$settings->skype}}</td>
              </tr>
             
               <tr>
                <td>email1</td>
                <td>{{$settings->email1}}</td>
              </tr>

               <tr>
                <td>email2</td>
                <td>{{$settings->email2}}</td>
              </tr>

               <tr>
                <td>facebook</td>
                <td>{{$settings->facebook}}</td>
              </tr>

               <tr>
                <td>twitter</td>
                <td>{{$settings->twitter}}</td>
              </tr>

               <tr>
                <td>youtube</td>
                <td>{{$settings->youtube}}</td>
              </tr>

               <tr>
                <td>google</td>
                <td>{{$settings->google}}</td>
              </tr>

               <tr>
                <td>snap</td>
                <td>{{$settings->snap}}</td>
              </tr>

               <tr  style="background-color: #b38fb1;">
                <td>logo</td>
                <td><img src="{{URL::to('/')}}/{{$settings->logo}}" height="150" width="200"></td>
              </tr>

              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
