@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="#">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">عن المدرسة</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">تعديل بيانات</a>
      </li>
    </ul>
  </div>
      @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>تعديل بيانات
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>

    </div>
    <div class="portlet-body form">
      {!! Form::open(['method'=>'PUT','url'=>'school/about_us/'.$about_us->id,'class'=>"form-horizontal" ,'role'=>"form"]) !!}
    
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-1 control-label"> عن المدرسة </label>
            <div class="col-md-10">
              <textarea id="editor1" name="details" rows="7"  class="form-control">{{$about_us->details}}</textarea>
            </div>
          </div>
          </div>
        <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
  <!-- END SAMPLE FORM PORTLET-->
  @stop
