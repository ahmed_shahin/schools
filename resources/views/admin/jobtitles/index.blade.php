@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/school')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/school/jobtitles')}}">المسيات الوظيفية</a>
            <i class="fa fa-angle-right"></i>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th> الاسم بالعربى</th>
                <th> الاسم بالانجليزي</th>
                <th>تعديل</th>
                <th>حذف</th>
            </tr>
        </thead>
    
        <tbody>
       @foreach($jobtitles as $jobtitle)
               <tr>
                  <td>{{$jobtitle->ar_name}}</td>
                   <td>{{$jobtitle->en_name}}</td>
                  <td><a href="{{URL::to('/')}}/school/jobtitles/{{$jobtitle->id}}/edit" class="btn  btn-primary"><i class="fa fa-pencil-square-o btn  btn-primary "></i>تعديل</a></td>
                  <td>
                   {{Form::open(['method'=>'DELETE', 'action'=>['Admin\JobTitlesController@destroy', $jobtitle->id], 'id'=>'form','class'=>"btn btn-danger"]) }}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$jobtitle->ar_name}}؟'))
                                $(this).closest('form').submit();"><i class="fa fa-trash-o btn  btn-danger "></i> حذف</a>
                                {{Form::close()}}
                 </td>
                </tr>
                @endforeach
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop


