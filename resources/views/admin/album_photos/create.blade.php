@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('school')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">صور الالبومات</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة صور الالبوم</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة صور الالبوم
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a> 
      </div>
    </div>
    <div class="portlet-body form">
      {!! Form::open(['method'=>'post','url'=>'school/albumPhotos','class'=>"form-horizontal" ,'role'=>"form",'files'=>'true']) !!}
      @if(Session::has('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
      @endif
        <div class="form-body">
         <div class="form-group">
           <label class="col-md-1 control-label"> الاسم </label>
            <div class="col-md-10">
                      {{ Form::select('album_id', $countries,null,['class'=>'form-control']) }}
                    <p class='text-danger'>{{$errors->first('album_id')}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label"> الاسم </label>
            <div class="col-md-10">
                   <input class="form-control" type="file" name="photo">         
                    <p class='text-danger'>{{$errors->first('photo')}}</p>
            </div>
          </div>

          </div>
        <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
  <!-- END SAMPLE FORM PORTLET-->
  @stop

