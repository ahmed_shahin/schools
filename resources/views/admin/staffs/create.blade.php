@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('school')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('staff')}}">هيئة التدريس</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة مادة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة هيئة تدريس
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'school/staffs' ,'role'=>"form"]) !!}
             <center><b>بيانات الفصل</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> الاسم بالعربية </label>
            <div class="col-md-4">
                  {{Form::text('ar_name','',['class'=>"form-control",'placeholder'=>'الاسم بالعربية'])}}       
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>

            <label class="col-md-2 control-label"> الاسم بالانجليزية </label>
            <div class="col-md-4">
                  {{Form::text('en_name','',['class'=>"form-control",'placeholder'=>'الاسم بالانجليزية '])}}       
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>

             <label class="col-md-2 control-label">‫ المسمي الوظيفي </label>
            <div class="col-md-10">
          {!! Form::select('jobtitle_id',$jobtitles,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('jobtitle_id')}}</p>
            </div>

            <label class="col-md-2 control-label"> الجوال </label>
            <div class="col-md-4">
                  {{Form::text('phone','',['class'=>"form-control",'placeholder'=>' الجوال '])}}       
                    <p class='text-danger'>{{$errors->first('phone')}}</p>
            </div>

            <label class="col-md-2 control-label"> المكان </label>
            <div class="col-md-4">
                  {{Form::text('place','',['class'=>"form-control",'placeholder'=>' الجوال '])}}       
                    <p class='text-danger'>{{$errors->first('place')}}</p>
            </div>

            <label class="col-md-2 control-label"> رقم البطاقة </label>
            <div class="col-md-4">
                  {{Form::text('card','',['class'=>"form-control",'placeholder'=>' رقم البطاقة '])}}       
                    <p class='text-danger'>{{$errors->first('card')}}</p>
            </div>

            <label class="col-md-2 control-label"> الايميل </label>
            <div class="col-md-4">
                  {{Form::text('email','',['class'=>"form-control",'placeholder'=>' الايميل '])}}       
                    <p class='text-danger'>{{$errors->first('email')}}</p>
            </div>

            <label class="col-md-2 control-label">‫ النوع </label>
            <div class="col-md-10">
          {!! Form::select('type',$genders,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('type')}}</p>
            </div>


            <label class="col-md-2 control-label">‫ المادة </label>
            <div class="col-md-10">
          {!! Form::select('subject_id',$subjects,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('subject_id')}}</p>
            </div>

            <label class="col-md-2 control-label">‫ الجنسية </label>
            <div class="col-md-10">
          {!! Form::select('nationality_id',$nationalities,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('nationality_id')}}</p>
            </div>

            <label class="col-md-2 control-label">‫ المدن </label>
            <div class="col-md-10">
          {!! Form::select('city_id',$cities,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('city_id')}}</p>
            </div>

            <label class="col-md-2 control-label">‫ الدول </label>
            <div class="col-md-10">
          {!! Form::select('country_id',$countries,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('country_id')}}</p>
            </div>
             <label class="col-md-2 control-label">‫ اللغه </label>
            <div class="col-md-10">
          {!! Form::select('language_id',$languages,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('language_id')}}</p>
            </div>

             <label class="col-md-2 control-label">  ‫اسم المستخدم  </label>
            <div class="col-md-4">
                   {{Form::text('username','',['class'=>"form-control",'placeholder'=>'اسم المستخدم'])}}       
                    <p class='text-danger'>{{$errors->first('username')}}</p>
            </div>
            <label class="col-md-2 control-label"> كلمة المرور </label>
            <div class="col-md-4">
                   {{Form::password('password',['class'=>"form-control",'placeholder'=>'كلمة المرور'])}}       
                    <p class='text-danger'>{{$errors->first('password')}}</p>
            </div>           
         
            
          </div>
          </div> 
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
