@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    المعرض <small>قائمة المعرض</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/school')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/school/staffs')}}">هيئه الموظفين</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                 <th> <strong> اسم بالعربي </strong></th>
                 <th> <strong> اسم بالانجليزي </strong></th>
                  <th> <strong> اسم المسمي الوظيفي</strong></th>
                   <th> <strong> الجوال </strong></th>
                    <th> <strong> الايميل</strong></th>
                    <th> <strong> البطاقة</strong></th>
                    <th> <strong> المادة</strong></th>
                    <th> <strong> الجنسية</strong></th>
                     <th> <strong> اللغة الام</strong></th>
                   {{--   <th> <strong> البلد</strong></th>
                     <th> <strong> المدينة</strong></th> --}}
                  
            </tr>
        </thead>
    
        <tbody>
               @foreach($staffs as $staff)

                  <tr>
                    <td class="v-align-middle" ><a href="{{URL::to('/')}}/school/staffs/{{$staff->id}}">
                    {{$staff->ar_name}}</a></td>
                    <td class="v-align-middle" ><a href="{{URL::to('/')}}/school/staffs/{{$staff->id}}">
                    {{$staff->en_name}}</a></td>
                    @if(isset($staff->jobtitle->ar_name))
                    <td class="v-align-middle" >{{$staff->jobtitle->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمسمي الوظيفي</td>
                    @endif

                     <td class="v-align-middle" >{{$staff->phone}}</td>
                     <td class="v-align-middle" >{{$staff->email}}</td>
                     <td class="v-align-middle" >{{$staff->card}}</td>

                    @if(isset($staff->subject->ar_name))
                    <td class="v-align-middle" >{{$staff->subject->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمادة</td>
                    @endif

                    @if(isset($staff->nationality->ar_name))
                    <td class="v-align-middle" >{{$staff->nationality->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالجنسية</td>
                    @endif

                    @if(isset($staff->language->ar_name))
                    <td class="v-align-middle" >{{$staff->language->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالغه</td>
                    @endif

                   {{--  @if(isset($staff->country->name))
                    <td class="v-align-middle" >{{$staff->country->name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالبلد</td>
                    @endif

                    @if(isset($staff->city->name))
                    <td class="v-align-middle" >{{$staff->city->name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمدينة</td>
                    @endif --}}


                    
                  <td>
                  <a href="{{URL::to('/')}}/school/staffs/{{$staff->id}}/edit" class="btn  btn-warning">تعديل</a>
                  

                  {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\StaffsController@destroy', $staff->id, 'id'=>'form','class'=>"btn btn-danger"]]) !!}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$staff->ar_name}}؟'))
                                $(this).closest('form').submit();"> حذف</a>
                                {{Form::close()}}
                              </td>
                                  </tr>
                  @endforeach              
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop
@section('jsCode')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop


