@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">المحافظات</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">تعديل محافظة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اعديل محافظة
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body form">
      {!! Form::open(['method'=>'PUT','url'=>'admin/city/'.$city->id,'class'=>"form-horizontal" ,'role'=>"form"]) !!}
      @if(Session::has('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
      @endif
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-1 control-label"> الاسم </label>
            <div class="col-md-10">
                   <input class="form-control" type="text" name="name" placeholder="اسم المحافظة" value="{{$city->name}}">         
                    <p class='text-danger'>{{$errors->first('name')}}</p>
            </div>
          </div>
          </div>

                   <div class="form-body">
          <div class="form-group">
            <label class="col-md-1 control-label"> الاسم </label>
            <div class="col-md-10">
                   {!! Form::select('country',$counties,$city->country_id,['class'=>"form-control",'required']) !!}         
                    <p class='text-danger'>{{$errors->first('name')}}</p>
            </div>
          </div>
          </div>
        <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
        {!! Form::close() !!}
    </div>
  </div>

  
  
    @stop
