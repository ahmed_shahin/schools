<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Nano Schools </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{URL::to('/')}}/assets/admin/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{URL::to('/')}}/assets/admin/global/css/components-md-rtl.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/css/plugins-md-rtl.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/layout/css/layout-rtl.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{URL::to('/')}}/assets/admin/layout/css/themes/darkblue-rtl.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/layout/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

<link href="{{URL::to('/')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

 @yield('styleCode')
<link rel="shortcut icon" href="favicon.ico"/>
<link href='https://fonts.googleapis.com/css?family=Cairo' rel='stylesheet'>
<style>
body {
    font-family: 'Cairo';font-size: 16px;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-md page-header-fixed page-quick-sidebar-over-content">
<!-- BEGIN HEADER -->
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="#">
			<img src="{{URL::to('/')}}/assets/admin/layout/img/logmo.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">

				<!-- BEGIN TODO DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

				<!-- END TODO DROPDOWN -->
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" class="img-circle" src="{{URL::to('/')}}/{{@Auth::user()->image}}"/>
					<span class="username username-hide-on-mobile">
					{{@Auth::user()->name}} </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							
								@if(Auth::guard('admin')->check())
								<a href="{{URL::to('admin/logout')}}">
								@elseif(Auth::guard('school')->check())
								<a href="{{URL::to('school/logout')}}">
								@endif
							<i class="icon-key"></i>تسجيل خروج </a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->

					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
		@if(Request::is('admin') || Request::is('admin/*'))
			<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">الشركات</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/admin/companies/create')}}">
							<i class="fa fa-plus"></i>
							اضافة شركة</a>
						</li>
						<li>
							<a href="{{URL::to('/admin/companies')}}">
							<i class="fa fa-list"></i>
							الشركات</a>
						</li>
					</ul>
				</li>

				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">المدارس</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/admin/schools/create')}}">
							<i class="fa fa-plus"></i>
							اضافة مدرسة</a>
						</li>
						<li>
							<a href="{{URL::to('/admin/schools')}}">
							<i class="fa fa-list"></i>
							المدارس</a>
						</li>
					</ul>
				</li>

					<li class=" start ">
					<a href="javascript:;">
					<i class="fa fa-globe "></i>
					<span class="title">الدول</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/admin/country/create')}}">
							<i class="fa fa-plus"></i>
							اضافة دولة </a>
						</li>
						<li>
							<a href="{{URL::to('/admin/country')}}">
							<i class="fa fa-list"></i>
							الدول </a>
						</li>
						
					</ul>
				</li>
				<li class=" start ">
					<a href="javascript:;">
					<i class="fa fa-globe "></i>
					<span class="title">المدن</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/admin/city/create')}}">
							<i class="fa fa-plus"></i>
							اضافة مدينة </a>
						</li>
						<li>
							<a href="{{URL::to('/admin/city')}}">
							<i class="fa fa-list"></i>
							المدن </a>
						</li>
						
					</ul>
		           </li>
		           <li class=" start ">
					<a href="javascript:;">
					<i class="fa fa-globe "></i>
					<span class="title">الجنسيات</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/admin/nationalities/create')}}">
							<i class="fa fa-plus"></i>
							اضافة جنسية </a>
						</li>
						<li>
							<a href="{{URL::to('/admin/nationalities')}}">
							<i class="fa fa-list"></i>
							جميع الجنسيات </a>
						</li>
						
					</ul>
				</li>

				<li class=" start ">
					<a href="javascript:;">
					<i class="fa fa-globe "></i>
					<span class="title">اللغات</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/admin/languages/create')}}">
							<i class="fa fa-plus"></i>
							اضافة لغة </a>
						</li>
						<li>
							<a href="{{URL::to('/admin/languages')}}">
							<i class="fa fa-list"></i>
							جميع اللغات </a>
						</li>
						
					</ul>
				</li>


				@endif
				@if(Request::is('school') || Request::is('school/*'))
			<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">المراحل الدراسية</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/schoolGrades/create')}}">
							<i class="fa fa-plus"></i>
							اضافة مرحلة</a>
						</li>
						<li>
							<a href="{{URL::to('/school/schoolGrades')}}">
							<i class="fa fa-list"></i>
							المراحل الدراسية</a>
						</li>
					</ul>
				</li>

				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">الصفوف الدراسية</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/classrooms/create')}}">
							<i class="fa fa-plus"></i>
							اضافة صف دراسي</a>
						</li>
						<li>
							<a href="{{URL::to('/school/classrooms')}}">
							<i class="fa fa-list"></i>
							الصفوف الدراسية</a>
						</li>
					</ul>
				</li>



				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">الفصول الدراسية</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/semesters/create')}}">
							<i class="fa fa-plus"></i>
							اضافة فصل دراسي</a>
						</li>
						<li>
							<a href="{{URL::to('/school/semesters')}}">
							<i class="fa fa-list"></i>
							الفصول الدراسية</a>
						</li>
					</ul>
				</li>

				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">المواد الدراسية</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/subjects/create')}}">
							<i class="fa fa-plus"></i>
							اضافة مادة دراسي</a>
						</li>
						<li>
							<a href="{{URL::to('/school/subjects')}}">
							<i class="fa fa-list"></i>
							المواد الدراسية</a>
						</li>
					</ul>
				</li>

				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">هيئة التدريس</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/staffs/create')}}">
							<i class="fa fa-plus"></i>
							اضافة هيئة تدريس</a>
						</li>
						<li>
							<a href="{{URL::to('/school/staffs')}}">
							<i class="fa fa-list"></i>
							هيئة التدريس</a>
						</li>
					</ul>
				</li>

				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">الطلاب</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/students/create')}}">
							<i class="fa fa-plus"></i>
							اضافة طالب جديد</a>
						</li>
						<li>
							<a href="{{URL::to('/school/students')}}">
							<i class="fa fa-list"></i>
							كل الطلاب</a>
						</li>
					</ul>
				</li>

				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">المسميات الوظيفية</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/jobtitles/create')}}">
							<i class="fa fa-plus"></i>
							اضافة مسمي وظيفي</a>
						</li>
						<li>
							<a href="{{URL::to('/school/jobtitles')}}">
							<i class="fa fa-list"></i>
							المسميات الوظيفية</a>
						</li>
					</ul>
				</li>

				<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">حصص المرحلة</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						{{-- <li>
							<a href="{{URL::to('/school/classrooms/create')}}">
							<i class="fa fa-plus"></i>
							اضافة صف دراسي</a>
						</li> --}}
						<li>
							<a href="{{URL::to('/school/lesons')}}">
							<i class="fa fa-list"></i>
							مشاهدة الحصص</a>
						</li>
					</ul>
				</li>

					<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">الالبومات</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/albums/create')}}">
							<i class="fa fa-plus"></i>
							اضافة البوم </a>
						</li>
						<li>
							<a href="{{URL::to('/school/albums')}}">
							<i class="fa fa-list"></i>
							الالبومات</a>
						</li>
					</ul>
				   </li>

					
					<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">صور الالبومات </span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/albumPhotos/create')}}">
							<i class="fa fa-plus"></i>
							اضافة صور البوم </a>
						</li>
						<li>
							<a href="{{URL::to('/school/albumPhotos')}}">
							<i class="fa fa-list"></i>
							صور الالبومات </a>
						</li>
					</ul>
				   </li>

				 <li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">اختبارات الترم </span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/tests/create')}}">
							<i class="fa fa-plus"></i>
							اضافة اختبار </a>
						</li>
						<li>
							<a href="{{URL::to('/school/tests')}}">
							<i class="fa fa-list"></i>
							عرض الاختبارات </a>
						</li>
					</ul>
				   </li>

				   <li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">نتائج الترم </span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/results/create')}}">
							<i class="fa fa-plus"></i>
							اضافة نتيجة </a>
						</li>
						<li>
							<a href="{{URL::to('/school/results')}}">
							<i class="fa fa-list"></i>
							عرض النتائج </a>
						</li>
					</ul>
				   </li>

				   <li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">اسابيع المدرسة </span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/weeks/create')}}">
							<i class="fa fa-plus"></i>
							اضافة اسبوع </a>
						</li>
						<li>
							<a href="{{URL::to('/school/weeks')}}">
							<i class="fa fa-list"></i>
							عرض الاسابيع </a>
						</li>
					</ul>
				   </li>

				   <li class="start ">
					<a href="javascript:;">
					<i class="fa fa-home"></i>
					<span class="title">جدول المدرسة </span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{URL::to('/school/schedules/create')}}">
							<i class="fa fa-plus"></i>
							اضافة اختبار </a>
						</li>
						<li>
							<a href="{{URL::to('/school/schedules')}}">
							<i class="fa fa-list"></i>
							عرض الاختبارات </a>
						</li>
					</ul>
				   </li>



				   	<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-list"></i>
					<span class="title">عن المدرسة </span>

					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						
						<li>
							

							<a href="{{URL::to('/school/settings/create')}}">
							<i class="fa fa-list"></i>
							اضافة </a>
						</li>
						<li>
							<a href="{{URL::to('/school/settings/edit')}}">
							<i class="fa fa-list"></i>
							تعديل </a>

						</li>
					</ul>
				   </li>



				   



				    <li>
							<a href="#">
							<i class="fa fa-list"></i>
							التقويم الدراسى </a>
						</li>

				   <li>
							<a href="#">
							<i class="fa fa-envelope"></i>
							رسائل التواصل </a>
						</li>

				@endif
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
		          @yield('content')
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	<a href="javascript:;" class="page-quick-sidebar-toggler"><i class="icon-close"></i></a>

	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2018 &copy;  <a href="https://www.linkedin.com/in/amr-mahanna/" title="amr mahanna" style="color: #fff" target="_blank">amr mahanna.</a>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/respond.min.js"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>


<script src="{{URL::to('/')}}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

        <script src="{{URL::to('/')}}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="{{URL::to('/')}}/assets/admin/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable( {
        "scrollX": true
    } );
} );
</script>
<script>

      jQuery(document).ready(function() {
         Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
      });
   </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
