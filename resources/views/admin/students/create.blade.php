@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">انشاء طلاب</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة طالب</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة طالب جديد
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'school/students' ,'role'=>"form",'files'=>'true']) !!}
             <center><b>بيانات الطالب</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
          <div class="row">

            <label class="col-md-2 control-label"> الاسم </label>
            <div class="col-md-4">
                  {{Form::text('name','',['class'=>"form-control",'placeholder'=>'الاسم'])}}       
                    <p class='text-danger'>{{$errors->first('name')}}</p>
            </div>

            <label class="col-md-2 control-label"> صورة الطالب</label>
            <div class="col-md-4">
                   {{Form::file('img',['class'=>"form-control"])}}       
                    <p class='text-danger'>{{$errors->first('img')}}</p>
            </div>

              <label class="col-md-2 control-label"> تاريخ الميلاد </label>
            <div class="col-md-4">
                  {{Form::date('birthdate','',['class'=>"form-control",'placeholder'=>'تاريح الميلاد'])}}       
                    <p class='text-danger'>{{$errors->first('birthdate')}}</p>
            </div>

              <label class="col-md-2 control-label"> تاريخ الالتحاق بالمدرسة </label>
            <div class="col-md-4">
                  {{Form::date('enrollmentdate','',['class'=>"form-control",'placeholder'=>'تاريخ الالتحاق بالمدرسة'])}}       
                    <p class='text-danger'>{{$errors->first('enrollmentdate')}}</p>
            </div>

            <label class="col-md-2 control-label">‫ النوع </label>
            <div class="col-md-10">
          {!! Form::select('gender',$genders,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('gender')}}</p>
            </div>

             <label class="col-md-2 control-label">‫ المرحلة  الدراسية </label>
            <div class="col-md-10">
          {!! Form::select('grade_id',$grades,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('grade_id')}}</p>
            </div>


             <label class="col-md-2 control-label">‫ الصف الدراسي  </label>
            <div class="col-md-10">
          {!! Form::select('classroom_id',$rooms,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('classroom_id')}}</p>
            </div>


                <label class="col-md-2 control-label">‫ الفصل الدراسي   </label>
            <div class="col-md-10">
          {!! Form::select('semester_id',$semesters,null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('semester_id')}}</p>
            </div>

            
           

         
     
           
         
            
          </div>
          </div> 
        </div>

         
           <div class="form-body">
          <center><b> بيانات ولى الامر</b></center><br>
          <div class="form-group">
          <div class="row">
            <label class="col-md-2 control-label"> اسم ولي الامر  </label>
            <div class="col-md-4">
                   {{Form::text('fathername','',['class'=>"form-control",'placeholder'=>' ‫اسم ولي الامر'])}}       
                    <p class='text-danger'>{{$errors->first('fathername')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫رقم‬ ‫‫‫الجوال‬  </label>
            <div class="col-md-4">
                   {{Form::text('phone','',['class'=>"form-control",'placeholder'=>'‫رقم‬ ‫‫‫الجوال‬'])}}       
                    <p class='text-danger'>{{$errors->first('phone')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫البريد الالكتروني  </label>
            <div class="col-md-4">
                   {{Form::text('email','',['class'=>"form-control",'placeholder'=>' ‫البريد الالكتروني'])}}       
                    <p class='text-danger'>{{$errors->first('email')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫اسم المستخدم  </label>
            <div class="col-md-4">
                   {{Form::text('username','',['class'=>"form-control",'placeholder'=>'اسم المستخدم'])}}       
                    <p class='text-danger'>{{$errors->first('username')}}</p>
            </div>
            <label class="col-md-2 control-label"> كلمة المرور </label>
            <div class="col-md-4">
                   {{Form::password('password',['class'=>"form-control",'placeholder'=>'كلمة المرور'])}}       
                    <p class='text-danger'>{{$errors->first('password')}}</p>
            </div>

            <label class="col-md-2 control-label">‫ صفة القرابة </label>
            <div class="col-md-4">
          {!! Form::select('relation_type',['اختار صفة القرابة','اب','ام','اخ','اخت'],null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('relation_type')}}</p>
            </div>
            <label class="col-md-2 control-label">‫ البلد </label>
            <div class="col-md-4">
          {!! Form::select('country_id',['saudi arabia','egypt'],null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('country_id')}}</p>
            </div>
            <label class="col-md-2 control-label">‫ المدينة </label>
            <div class="col-md-4">
          {!! Form::select('city_id',['mansoura','egypt'],null,['class'=>"form-control"]) !!}
                      <p class='text-danger'>{{$errors->first('city_id')}}</p>
            </div>
            <label class="col-md-2 control-label">  ‫الشارع  </label>
            <div class="col-md-10">
                   {{Form::textarea('street','',['class'=>"form-control",'placeholder'=>'عنوان الشارع'])}}       
                    <p class='text-danger'>{{$errors->first('street')}}</p>
            </div>
          </div>
          </div>
          </div>

           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
