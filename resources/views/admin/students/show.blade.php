@extends('admin.layout')

 @section('content')

 
          
    <div class="clearfix"></div>
    <div class="content">
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
           <center>
           <b> بيانات الطالب </b>
            </center>
            
<a href="{{URL::to('/')}}/school/students/{{$students->id}}/edit" class="btn  btn-warning"> تعديل بيانات الفصل الدراسي </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="grid-body ">
              <table id="example1" style="direction: rtl;" class="table table-bordered table-striped">

              <th style="text-align:center;"> بيانات الطالب </th>
              <th style="text-align:center;"> ***** بيانات الطالب *****</th>

              <tr>
                <td>الاسم الطالب</td>
                <td>{{$students->name}}</td>
              </tr>

               <tr>
                <td>تاريخ الميلاد</td>
                <td>{{$students->birthdate}}</td>
              </tr>

              <tr>
                <td>تاريخ الالتحاق بالمدرسة</td>
                <td>{{$students->enrollmentdate}}</td>
              </tr>

              <tr>
                <td>النوع</td>
                <td>{{$students->gender}}</td>
              </tr>

              <tr>
                <td>اسم المرحلة</td>
                <td>{{$students->grade->ar_name}}</td>
              </tr>

              <tr>
                <td>اسم الصف</td>
                <td>{{$students->classroom->ar_name}}</td>
              </tr>

              <tr>
                <td>اسم الفصل</td>
                <td>{{$students->semester->ar_name}}</td>
              </tr>

               <tr  style="background-color: #ccc;">
                <td>صورة الطالب</td>
                <td><img src="{{URL::to('/')}}/{{$students->img}}" height="150" width="200"></td>
              </tr>

               

                 
              </table>
            </div>
           </div>
        </div>
      </div>
        </div>

@stop
