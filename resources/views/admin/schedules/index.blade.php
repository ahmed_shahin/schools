@extends('admin.layout')
@section('styleCode')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
@stop
@section('content')
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    المعرض <small>قائمة المعرض</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{URL::to('/school')}}">الرئيسيه</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{URL::to('/school/schedules')}}">الجدول</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
  @if(Session::has('success'))
  <div class="alert alert-success">{{Session::get('success')}}</div>
  @endif
    <!-- END EXAMPLE TABLE PORTLET-->
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                 <th> <strong> اسم المرحلة </strong></th>
                 <th> <strong> اسم الصف </strong></th>
                  <th> <strong> اسم الفصل</strong></th>
                   <th> <strong> اسم الاسبوع</strong></th>
                   <th> <strong> اسم اليوم </strong></th>
                    <th> <strong> اسم الحصة</strong></th>
                    <th> <strong>اسم المادة</strong></th>
                    <th> <strong> اسم المدرس</strong></th>
                    <th> <strong> الترايخ</strong></th>
                     
                 
                  
            </tr>
        </thead>
    
        <tbody>
               @foreach($schedules as $schedule)

                  <tr>
                    

                    @if(isset($schedule->grade->ar_name))
                    <td class="v-align-middle" >{{$schedule->grade->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمرحلة</td>
                    @endif

                    @if(isset($schedule->room->ar_name))
                    <td class="v-align-middle" >{{$schedule->room->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالصف</td>
                    @endif

                    @if(isset($schedule->semester->ar_name))
                    <td class="v-align-middle" >{{$schedule->semester->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالترم</td>
                    @endif

                    @if(isset($schedule->week->ar_name))
                    <td class="v-align-middle" >{{$schedule->week->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالاسبوع</td>
                    @endif

                    @if(isset($schedule->day->name))
                    <td class="v-align-middle" >{{$schedule->day->name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمسمي الوظيفي</td>
                    @endif

                    @if(isset($schedule->leson->name))
                    <td class="v-align-middle" >{{$schedule->leson->name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمسمي الوظيفي</td>
                    @endif

                    @if(isset($schedule->subject->ar_name))
                    <td class="v-align-middle" >{{$schedule->subject->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمسمي الوظيفي</td>
                    @endif

                    @if(isset($schedule->staff->ar_name))
                    <td class="v-align-middle" >{{$schedule->staff->ar_name}}</td>
                    @else
                    <td class="v-align-middle" >غير مرتبط بالمسمي الوظيفي</td>
                    @endif

                    

                     <td class="v-align-middle" >{{$schedule->date}}</td>
                  

                    


                    
                  <td>
                  <a href="{{URL::to('/')}}/school/schedules/{{$schedule->id}}/edit" class="btn  btn-warning">تعديل</a>
                  

                  {!! Form::open(['method'=>'DELETE', 'action'=>['Admin\SchedulesController@destroy', $schedule->id, 'id'=>'form','class'=>"btn btn-danger"]]) !!}
                                <a href="javascript:;" style="color: #fff;" onclick="if (confirm('حذف {{$schedule->date}}؟'))
                                $(this).closest('form').submit();"> حذف</a>
                                {{Form::close()}}
                              </td>
                                  </tr>
                  @endforeach              
        </tbody>
    </table>
    <!-- END EXAMPLE TABLE PORTLET-->
@stop
@section('jsCode')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop


