@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('school')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('school/jobtitles')}}">المسيات الوظيفية</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">تعديل المنطقة</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> تعديل المسمي الوظيفي
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body form">
      {!! Form::open(['method'=>'PUT','url'=>'school/jobtitles/'.$jobtitles->id,'class'=>"form-horizontal" ,'role'=>"form"]) !!}
      @if(Session::has('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
      @endif
        <div class="form-body">
          <div class="form-group">
              <label class="col-md-1 control-label"> الاسم بالعربى </label>
            <div class="col-md-4">
                   <input class="form-control" type="text" name="ar_name" value="{{$jobtitles->ar_name}}" placeholder="اسم المسمي الوظيفي بالعربى">         
                    <p class='text-danger'>{{$errors->first('ar_name')}}</p>
            </div>

            <label class="col-md-1 control-label"> الاسم بالانجليزي</label>
            <div class="col-md-4">
                   <input class="form-control" type="text" name="en_name" value="{{$jobtitles->en_name}}" placeholder="اسم المسمي الوظيفي بالانجليزي">         
                    <p class='text-danger'>{{$errors->first('en_name')}}</p>
            </div>
          </div>
          </div>
       

          
        <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
  <!-- END SAMPLE FORM PORTLET-->
  @stop
  @section('jsCode')
 {{--  <script src="{{ URL::to('/') }}/assets/admin/ckeditor/ckeditor.js"></script>

  <script>
      CKEDITOR.replace( 'editor1' );
      CKEDITOR.config.extraPlugins = 'justify';
  </script> --}}
  
  
    @stop
