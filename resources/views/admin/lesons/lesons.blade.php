@extends('admin.layout')
  @section('content')
   <!-- BEGIN PAGE HEADER-->
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{URL::to('admin')}}">الرئيسيه</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{URL::to('user')}}">جدول الحصح</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">اضافة حصص</a>
      </li>
    </ul>
  </div>
   @if(Session::has('flash_message'))

<div id="flash_message" class="alert alert-success">
  <strong>
                    {{Session::get('flash_message')}}
  </strong>
</div>
@endif
  <!-- END PAGE HEADER-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box purple ">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> اضافة حصص
      </div>
      <div class="tools">
        <a href="" class="collapse">
        </a>

        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div>
    </div>
    <div class="portlet-body">
    <br>
      {!! Form::open(['method'=>'post','url'=>'school/lesons' ,'role'=>"form"]) !!}
             <center><b>بيانات الحصص</b></center><br>
              
        <div class="form-body">
          <div class="form-group">
           @for ($i = 1; $i <= $lesonsID; $i++) 
          <div class="row">
                
                 <input type="hidden" name="lesonid" value="{{$lesonsID}}">
                 <input type="hidden" name="gradeid" value="{{$gradeId}}">

            <label class="col-md-2 control-label"> اسم الحصة </label>
            <div class="col-md-4">
                  {{Form::text('name'.$i,'',['class'=>"form-control",'placeholder'=>'اسم الحصة'])}}       
                    <p class='text-danger'>{{$errors->first('name')}}</p>
            </div>
             <label class="col-md-2 control-label"> اليوم </label>
            <div class="col-md-4">
                  {{Form::text('day'.$i,'',['class'=>"form-control",'placeholder'=>'اليوم '])}}       
                    <p class='text-danger'>{{$errors->first('day')}}</p>
            </div>

            <label class="col-md-2 control-label"> من </label>
            <div class="col-md-4">
                  {{Form::text('from'.$i,'',['class'=>"form-control",'placeholder'=>'اليوم '])}}       
                    <p class='text-danger'>{{$errors->first('from')}}</p>
            </div>

            <label class="col-md-2 control-label"> الي </label>
            <div class="col-md-4">
                  {{Form::text('to'.$i,'',['class'=>"form-control",'placeholder'=>'اليوم '])}}       
                    <p class='text-danger'>{{$errors->first('to')}}</p>
            </div>
            
          </div>
          <hr>
          @endfor
          </div> 
           <center>
         <div class="form-actions right1">
          <button type="submit" class="btn green">حفظ</button>
        </div>
       </center>      
        {!! Form::close() !!}
    </div>
        </div>

    @stop
