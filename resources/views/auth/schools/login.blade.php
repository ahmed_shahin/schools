<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title> schools | تسجيل الدخول</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{URL::to('/')}}/assets/admin/pages/css/login-rtl.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="{{URL::to('/')}}/assets/global/css/components-md-rtl.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/global/css/plugins-md-rtl.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/layout/css/layout-rtl.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/assets/admin/layout/css/themes/darkblue-rtl.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{URL::to('/')}}/assets/admin/layout/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<link href='https://fonts.googleapis.com/css?family=Cairo' rel='stylesheet'>
<style>
body {
    font-family: 'Cairo';font-size: 18px;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->

<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->

<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4  content">

<div class="logo">
        <h3 class="form-title">تسجيل دخول المدارس </h3>

    <a href="">
  <img src="{{URL::to('/')}}/assets/admin/img/nano.png" width="190" height="90">
    </a>
</div>
    <!-- BEGIN LOGIN FORM -->
<!-- HTML Collective  -->
  @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if(Session::has('error'))
  <div class="alert alert-danger">
    <ul>
      <li>{{Session::get('error')}}</li>
    </ul>
  </div>
  @endif
  {!! Form::open(['url' => 'school/login','method'=>'post']) !!}
  {!! csrf_field() !!}
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">البريد الالكترونى</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="البريد الالكترونى" name="email"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">كلمة المرور</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="كلمة المرور" name="password"/>
        </div>
        <div class="form-actions">
            <center><button type="submit" class="btn btn-success uppercase">دخول</button></center>
        </div>

    {!! Form::close() !!}
<!-- HTML Collective  -->
    <!-- END LOGIN FORM -->
</div>
<div class="col-md-4"></div>

</div>

<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{URL::to('/')}}/assets/global/plugins/respond.min.js"></script>
<script src="{{URL::to('/')}}/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{URL::to('/')}}/assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{URL::to('/')}}/assets/admin/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Login.init();
Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
