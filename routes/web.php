<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::group([ 'namespace' => 'Admin', 'prefix' => 'admin'], function (){
	Route::get('login', 'AdminLoginController@showLoginForm');
	Route::post('login', 'AdminLoginController@login');
	Route::get('logout', 'AdminLoginController@logout');
});

Route::group([ 'namespace' => 'Admin', 'prefix' => 'school'], function (){
	Route::get('login', 'SchoolsLoginController@showLoginForm');
	Route::post('login', 'SchoolsLoginController@login');
	Route::get('logout', 'SchoolsLoginController@logout');
});



Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'checkadmin'], function (){
	Route::get('/', 'SettingsController@home');
	Route::resource('companies','CompaniesController');
	Route::resource('schools','SchoolsController');
	Route::resource('users','UsersController');
    Route::resource('country','CountriesController');
    Route::resource('nationalities','NationalitiesController');
    Route::resource('languages','LanguagesController');
    Route::resource('city','CityController');
});


Route::group(['namespace' => 'Admin', 'prefix' => 'school', 'middleware' => 'admin'], function (){	
	Route::resource('schoolGrades','SchoolGradesController');
	Route::resource('classrooms','ClassRoomsController');
	Route::resource('semesters','SemestersController');
	Route::resource('subjects','SubjectsController');
	Route::resource('students','StudentsController');
	Route::resource('lesons','LesonsController');
	Route::resource('staffs','StaffsController');
	Route::resource('tests','TestsController');
	Route::resource('weeks','WeeksController');
	Route::resource('results','ResultController');
	Route::resource('testresults','TestResultsController');
	Route::resource('schedules','SchedulesController');
	Route::resource('jobtitles','JobTitlesController');
	Route::get('addLesons/{id}', 'SchoolGradesController@addLesons');
	Route::get('addResults/{id}', 'ResultController@addResults');
	Route::get('/', 'SettingsController@home');
	Route::resource('albums','AlbumsController');
	Route::resource('albumPhotos','AlbumPhotosController');
	Route::get('settings/create', 'SettingsController@create');
	Route::post('settings/store', 'SettingsController@store');
	Route::get('settings/edit', 'SettingsController@edit');
    Route::put('settings/update/{id}', 'SettingsController@update');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
