<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Support\Translateable;
class Company extends Authenticatable
{
       protected $guarded = ['id'];
       protected $hidden = ['created_at','updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  protected $fillable = [
         'ar_name','en_name','commercial_no','owner_name','owner_phone','owner_email','owner_fax','country','city','street','location_details','officer_name','officer_phone','officer_email','officer_fax','contract','commercial_license','bank_data','ar_logo','en_logo'
    ];

    public function schools() {
return $this->hasMany('App\School');
}
  
}
