<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    //
     protected $fillable = [
        'ar_details','en_details'

    ];
}
