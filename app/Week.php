<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    protected $fillable = [
        'ar_name', 'en_name', 'from','to'
    ];


}
