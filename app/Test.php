<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
        'name', 'date', 'grade_id','room_id','semester_id','day_id'
    ];

     public function grade()
    {
    	return $this->belongsTo('App\SchoolGrades', 'grade_id');
    }

    public function room()
    {
    	return $this->belongsTo('App\Classroom', 'room_id');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester', 'semester_id');
    }

    public function day()
    {
    	return $this->belongsTo('App\Day', 'day_id');
    }


}
