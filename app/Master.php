<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Master extends Authenticatable
{
    protected $fillable = [
        'fathername', 'phone', 'email', 'relation_type', 'student_id', 'country_id', 'city_id', 'street','username','password',
    ];
}
