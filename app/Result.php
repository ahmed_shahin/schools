<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
        'grade_id', 'room_id','semester_id','test_id','student_id','result'
    ];

     public function grade()
    {
    	return $this->belongsTo('App\SchoolGrades', 'grade_id');
    }

    public function room()
    {
    	return $this->belongsTo('App\Classroom', 'room_id');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester', 'semester_id');
    }

    public function test()
    {
    	return $this->belongsTo('App\Test', 'test_id');
    }

	public function student()
    {
    	return $this->belongsTo('App\Student', 'student_id');
    }



}
