<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
     protected $fillable = [
        'ar_name', 'en_name',
    ];
}
