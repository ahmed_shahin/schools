<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'ar_name', 'en_name', 'ar_img', 'en_img', 'grade_id', 'classroom_id', 'semester_id',
    ];

    public function classroom()
    {
    	return $this->belongsTo('App\Classroom', 'classroom_id');
    }

    public function grade()
    {
    	return $this->belongsTo('App\SchoolGrades', 'grade_id');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester', 'semester_id');
    }
}
