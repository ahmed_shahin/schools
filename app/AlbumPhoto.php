<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumPhoto extends Model
{
	    protected $table = 'albumphotos';

     protected $fillable = [
        'photo',
        'album_id'
    ];

  public function album()
    {
      return $this->belongsTo('App\Album' , 'album_id' , 'id' );
    }
}
