<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
     protected $fillable = [
        'user_id','photo'
    ];

public function user() {
return $this->hasMany('App\User');
}

}
