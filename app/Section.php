<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
     protected $fillable = [
         'ar_name','en_name','photo'
    ];

public function user() {
return $this->hasMany('App\User');
}

}
