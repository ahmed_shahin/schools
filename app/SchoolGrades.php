<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolGrades extends Model
{
    protected $fillable = [
        'ar_name', 'en_name', 'manager','phone','email','office','transfer','school_id','from','to','lesons','break'
    ];

    public function school()
    {
    	return $this->belongsTo('App\School', 'school_id');
    }
}
