<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobTitles extends Model
{
     protected $fillable = [
        'ar_name', 'en_name',
    ];
}
