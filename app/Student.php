<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name', 'img', 'birthdate', 'enrollmentdate', 'gender', 'grade_id', 'classroom_id', 'semester_id',
    ];

    public function classroom()
    {
    	return $this->belongsTo('App\Classroom', 'classroom_id');
    }

    public function grade()
    {
    	return $this->belongsTo('App\SchoolGrades', 'grade_id');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester', 'semester_id');
    }
    
}
