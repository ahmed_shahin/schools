<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leson extends Model
{
    protected $fillable = [
        'name', 'day_id', 'from', 'to', 'grade_id','date','staff_id'
    ];


    public function day()
    {
        return $this->belongsTo('App\Day', 'day_id');
    }
    public function grade()
    {
    	return $this->belongsTo('App\SchoolGrades', 'grade_id');
    }

     public function staff()
    {
    	return $this->belongsTo('App\Staff', 'staff_id');
    }
}
