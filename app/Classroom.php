<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $fillable = [
        'ar_name', 'en_name', 'grade_id',
    ];

    public function grade()
    {
    	return $this->belongsTo('App\SchoolGrades', 'grade_id');
    }
}
