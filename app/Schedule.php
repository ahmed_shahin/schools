<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'date', 'grade_id', 'classroom_id','semester_id', 'week_id', 'day_id','staff_id','subject_id','leson_id'
    ];

    public function grade()
    {
    	return $this->belongsTo('App\SchoolGrades', 'grade_id');
    }

    public function room()
    {
    	return $this->belongsTo('App\Classroom', 'classroom_id');
    }

    public function week()
    {
        return $this->belongsTo('App\Week', 'week_id');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester', 'semester_id');
    }

    public function day()
    {
    	return $this->belongsTo('App\Day', 'day_id');
    }

     public function staff()
    {
    	return $this->belongsTo('App\Staff', 'staff_id');
    }

    public function subject()
    {
    	return $this->belongsTo('App\Subject', 'subject_id');
    }

}
