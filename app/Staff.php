<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Staff extends Authenticatable
{
    protected $table = "staffs";
    protected $fillable = [
        'ar_name', 'en_name', 'jobtitle_id', 'phone', 'place', 'card', 'email', 'type', 'subject_id',
        'nationality_id', 'language_id', 'country_id', 'city_id','username','password',
    ];

    public function jobtitle()
    {
    	return $this->belongsTo('App\JobTitles', 'jobtitle_id');
    }

    public function subject()
    {
    	return $this->belongsTo('App\Subject', 'subject_id');
    }

    public function nationality()
    {
    	return $this->belongsTo('App\Nationality', 'nationality_id');
    }

    public function language()
    {
    	return $this->belongsTo('App\Language', 'language_id');
    }

    public function country()
    {
    	return $this->belongsTo('App\Country', 'country_id');
    }

    public function city()
    {
    	return $this->belongsTo('App\City', 'city_id');
    }

}
