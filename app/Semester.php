<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $fillable = [
        'ar_name', 'en_name', 'classroom_id',
    ];

    public function classroom()
    {
    	return $this->belongsTo('App\Classroom', 'classroom_id');
    }

}
