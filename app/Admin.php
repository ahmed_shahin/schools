<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
class Admin extends Authenticatable
{

       protected $table='admins';
       protected $guard = 'admin';
       protected $hidden = ['password','created_at','updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  protected $fillable = [
         'name','email','password'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
