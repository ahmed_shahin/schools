<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'school_id',
        'about_us',
        'goals',
        'manager_letter',
        'mission'    ];

   
}
