<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\City;
use App\Section;
use App\Support\Translateable;
class User extends Authenticatable
{

       protected $table='users';
       protected $guarded = ['id'];
       protected $hidden = ['password', 'remember_token','created_at','updated_at'];
       protected $appends = ['section_ar_name','section_en_name','city_ar_name','city_en_name'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  protected $fillable = [
         'ar_name','en_name','ar_details','en_details','email','password','address','lat','lang','section_id','city_id','photo','phone','discount','type'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
