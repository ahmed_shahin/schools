<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Support\Translateable;
class School extends Authenticatable
{
       protected $guarded = ['id'];
       protected $hidden = ['password','created_at','updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  protected $fillable = [
         'ar_name','en_name','company_id','email','password','stages','specialization','curriculum_type','students_gender','manager_name','manager_phone','manager_email','manager_fax','secretary_name','secretary_phone','secretary_email','secretary_fax','officer_name','officer_phone','officer_email','officer_fax','agent_name','agent_phone','agent_email','agent_fax','country','city','street','location_details','lat','lang','ar_logo','en_logo','ar_logo_text','en_logo_text'
    ];

    public function company()
    {
      return $this->belongsTo('App\Company' , 'company_id' , 'id' );
    }
  
}
