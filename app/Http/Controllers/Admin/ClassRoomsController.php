<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\Classroom;
use App\SchoolGrades;
use Validator;
use Session;


class ClassRoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classrooms = Classroom::latest()->get();
        return view('admin.classrooms.index',  compact('classrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $grades=['' => 'اختر المرحلة الدراسية'] + SchoolGrades::pluck('ar_name', 'id')->all();
        //dd($schools);
        return view('admin.classrooms.create',compact('grades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'grade_id'=> 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

        $room = new Classroom($inputs);
        //dd($grade);
        if ($room->save()) {
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classrooms = Classroom::find($id);

       return View::make('admin.classrooms.show',compact('classrooms'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classrooms=Classroom::find($id);
        $grades=SchoolGrades::pluck('ar_name', 'id');

        return View::make('admin.classrooms.edit',compact('grades','classrooms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $classrooms = Classroom::find($id);

        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'grade_id'=> 'required'
        ]);

        if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();
        $classrooms->update($inputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rooms = Classroom::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
