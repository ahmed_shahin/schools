<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\Classroom;
use App\Semester;
use Validator;
use Session;


class SemestersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $semesters = Semester::latest()->get();
        return view('admin.semesters.index',  compact('semesters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms=['' => 'اختر الصف الدراسي'] + Classroom::pluck('ar_name', 'id')->all();
        //dd($schools);
        return view('admin.semesters.create',compact('rooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'classroom_id'=> 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

        $semester = new Semester($inputs);
        //dd($grade);
        if ($semester->save()) {
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $semesters = Semester::find($id);

       return View::make('admin.semesters.show',compact('semesters'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $semesters=Semester::find($id);
        $rooms=Classroom::pluck('ar_name', 'id');

        return View::make('admin.semesters.edit',compact('semesters','rooms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $semesters = Semester::find($id);

        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'classroom_id'=> 'required'
        ]);

        if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();
        $semesters->update($inputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $semesters = Semester::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
