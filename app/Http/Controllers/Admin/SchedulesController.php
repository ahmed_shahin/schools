<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\SchoolGrades;
use App\Staff;
use App\Classroom;
use App\Semester;
use App\Subject;
use App\Week;
use App\Leson;
use App\Day;
use App\Schedule;
use Validator;
use Session;


class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::latest()->get();
        return view('admin.schedules.index',  compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grades=['' => 'اختر المرحلة'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $subjects=['' => 'اختر المادة'] + Subject::pluck('ar_name', 'id')->all();
        $staffs=['' => 'اختر المدرس'] + Staff::pluck('ar_name', 'id')->all();
        $rooms=['' => 'اختر الفصل'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل'] + Semester::pluck('ar_name', 'id')->all();
        $lesons=['' => 'اختر الحصة'] + Leson::pluck('name', 'id')->all();
        $dayes=['' => 'اختر اليوم'] + Day::pluck('name', 'id')->all();
        $weeks=['' => 'اختر الاسبوع'] + Week::pluck('ar_name', 'id')->all();

        //dd($schools);
        return view('admin.schedules.create',compact('grades','subjects','weeks','staffs','rooms','semesters','lesons','dayes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make(request()->all(), [
        'date' => 'required',
        'grade_id' => 'required',
        'classroom_id' => 'required',
        'semester_id' => 'required',
        'week_id' => 'required',
        'day_id' => 'required',
        'staff_id' => 'required',
        'subject_id'=> 'required',
        'leson_id' => 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

        $schedule = new Schedule($inputs);
        //dd($grade);
        if ($schedule->save()) {
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedules = Schedule::find($id);

       return View::make('admin.schedules.show',compact('schedules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedules = Schedule::find($id);

        $grades=['' => 'اختر المرحلة'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $subjects=['' => 'اختر المادة'] + Subject::pluck('ar_name', 'id')->all();
        $staffs=['' => 'اختر المدرس'] + Staff::pluck('ar_name', 'id')->all();
        $rooms=['' => 'اختر الفصل'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل'] + Semester::pluck('ar_name', 'id')->all();
        $lesons=['' => 'اختر الحصة'] + Leson::pluck('name', 'id')->all();
        $dayes=['' => 'اختر اليوم'] + Day::pluck('name', 'id')->all();
        $weeks=['' => 'اختر الاسبوع'] + Week::pluck('ar_name', 'id')->all();
        
         return View::make('admin.schedules.edit',compact('schedules','grades','weeks','subjects','staffs','rooms','semesters','lesons','dayes'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $schedules = Schedule::find($id);

        $validator = Validator::make(request()->all(), [
        'date' => 'required',
        'grade_id' => 'required',
        'classroom_id' => 'required',
        'semester_id' => 'required',
        'week_id' => 'required',
        'day_id' => 'required',
        'staff_id' => 'required',
        'subject_id'=> 'required',
        'leson_id' => 'required'
        ]);

         if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();
        
        $schedules->update($inputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedules = Schedule::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
