<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\Http\Requests;
use View;
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App;
class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


  

    public function index()
    {
        $countries = Country::all();
       return View::make('admin.countries.index',compact('countries'));

    }


    public function create()
    {
        return View::make('admin.countries.create');
    }   

     public function store(Request $request)
    {    

   $validator = Validator::make($request->all(), [
        'name' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
       
        $data =Country::create([
        'name' => $request->input('name'),
        ]);  
       Session::flash('flash_message', 'تمت الاضافة بنجاح');

    return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country=Country::find($id);
       return View::make('admin.countries.edit',compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        'name' => 'required',
    
        ]);

        if ($validator->fails()) {
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $data =Country::where('id',$id)->update([
        'name' => $request->input('name')        ]);  
       Session::flash('flash_message', 'تم  التعديل بنجاح !');


    return redirect()->back();
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    $country=Country::find($id);
    $country->cities->delete();
    $country->delete();

        return redirect()->back();


    }

}
