<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

class AdminLoginController extends Controller
{   
   
    public function showLoginForm()
    {
        return view('auth.admin.login');

    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if(Auth::guard('admin')->attempt(['email'=> $request->email,'password' => $request->password ],$request->remember)){
            
                  return redirect('admin');
        }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('admin/login');

    }

}
