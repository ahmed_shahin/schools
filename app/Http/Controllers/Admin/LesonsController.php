<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\Leson;
use App\Day;
use App\SchoolGrades;
use Validator;
use Session;

class LesonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lesons = Leson::latest()->get();
        return view('admin.lesons.index',  compact('lesons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grades=['' => 'اختر مرحلة'] + SchoolGrades::pluck('ar_name', 'id')->all();
         $dayes=['' => 'اختر اليوم'] + Day::pluck('name', 'id')->all();
        //dd($schools);
        return view('admin.lesons.create',compact('grades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request);

        for ($i=1; $i <= request('lesonid'); $i++) { 
            # code...  
        $validator = Validator::make(request()->all(), [
        'name'.$i => 'required',
        'day_id' => 'required',
        'from'.$i => 'required|numeric',
        'to'.$i => 'required|numeric',
        'date'.$i => 'required',
        'staff_id' => 'required'
        ]);
      }

        // if ($validator->fails()) {
        //  return redirect()->back()->with('errors' , $validator->errors());
        // }

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();
        //$gradeid = SchoolGrades::where()->get();
        for ($j=1; $j <= request('lesonid'); $j++) { 
        $grade = new SchoolGrades($inputs);
         $leson = new Leson([
              'grade_id' => request('gradeid'),
              'name' => request('name'.$j),
              'day_id' => request('day_id'),
              'from' => request('from'.$j),
              'to' => request('to'.$j),
              'date' => request('date'.$j),
              'staff_id' => request('staff_id')
              ]);
            $leson->save();
        //dd($grade);
        }
       
            return redirect('school/schoolGrades');
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $lesons = Leson::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
