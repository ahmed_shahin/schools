<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\Classroom;
use App\Semester;
use App\SchoolGrades;
use App\Subject;
use Validator;
use Session;
use File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class SubjectsController extends Controller
{
    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(20) . '.' . $photo->guessClientExtension();
        $destinationPath ='uploads/';
        $photo->move($destinationPath, $fileName);
        return 'uploads/'.$fileName;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $subjects = Subject::latest()->get();
        return view('admin.subjects.index',  compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms=['' => 'اختر الصف الدراسي'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل الدراسي'] + Semester::pluck('ar_name', 'id')->all();
        $grades=['' => 'اختر المرحلة الدراسية'] + SchoolGrades::pluck('ar_name', 'id')->all();
        //dd($schools);
        return view('admin.subjects.create',compact('rooms','semesters','grades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'ar_img' => 'required',
        'en_img' => 'required',
        'grade_id' => 'required',
        'classroom_id' => 'required',
        'semester_id'=> 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

    
        if($request->hasFile('ar_img')){
        $inputs['ar_img']= $this->savePhoto($request->file('ar_img'));
        }
        if($request->hasFile('en_img')){
            $inputs['en_img']= $this->savePhoto($request->file('en_img'));
        }

        $subject = new Subject($inputs);
        //dd($grade);
        if ($subject->save()) {
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subjects = Subject::find($id);

       return View::make('admin.subjects.show',compact('subjects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subjects=Subject::find($id);
        $rooms=['' => 'اختر الصف الدراسي'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل الدراسي'] + Semester::pluck('ar_name', 'id')->all();
        $grades=['' => 'اختر المرحلة الدراسية'] + SchoolGrades::pluck('ar_name', 'id')->all();

        return View::make('admin.subjects.edit',compact('subjects','semesters','rooms','grades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subjects = Subject::find($id);

        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'ar_img' => 'required',
        'en_img' => 'required',
        'grade_id' => 'required',
        'classroom_id' => 'required',
        'semester_id'=> 'required'
        ]);

        if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();

       
       
       if($request->hasFile('ar_img')){
        $inputs['ar_img']= $this->savePhoto($request->file('ar_img'));
        }
        if($request->hasFile('en_img')){
            $inputs['en_img']= $this->savePhoto($request->file('en_img'));
        }



        $subjects->update($inputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subjects = Subject::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
