<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\Http\Requests;
use View;
use Illuminate\Support\Facades\Redirect;
use App\School;
use App\Company;
use File;
use Hash;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class SchoolsController extends Controller
{

   protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(20) . '.' . $photo->guessClientExtension();
        $destinationPath ='uploads/';
        $photo->move($destinationPath, $fileName);
        return 'uploads/'.$fileName;
    }



       public function index()
    {
       $schools=School::get();
       return View::make('admin.schools.index',compact('schools'));

    }

    

    public function create()
    {
    $companies=['' => 'اختر شركة'] + Company::pluck('ar_name', 'id')->all();
    return View::make('admin.schools.create',compact('companies'));

    }   

     public function store(Request $request)
    {    
        $data=$request->all();
        $validator = Validator::make($request->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'company_id' => 'required',
        'email' => 'required',
        'password' => 'required',
        'ar_logo' => 'required',
        'en_logo' => 'required',
        'country' => 'required',
        'city' => 'required',
        'street' => 'required',
        'location_details' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($request->hasFile('ar_logo')){
        $data['ar_logo']= $this->savePhoto($request->file('ar_logo'));
        }
        if($request->hasFile('en_logo')){
            $data['en_logo']= $this->savePhoto($request->file('en_logo'));
        }
        $data['password']=Hash::make($request->password);
        School::create($data);
       Session::flash('flash_message', 'تمت الاضافة بنجاح!');

   return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     $school=School::find($id);

       return View::make('admin.schools.show',compact('school'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school=School::find($id);
    $companies=Company::pluck('ar_name', 'id');

       return View::make('admin.schools.edit',compact('school','companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
    	$school = School::find($id);
        $validator = Validator::make($request->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'company_id' => 'required',
        'email' => 'required',
        'country' => 'required',
        'city' => 'required',
        'street' => 'required',
        'location_details' => 'required',
        ]);
             if($request->password != '')
        {
        $data=$request->all();
        $data['password'] = Hash::make($request->password);
        }else{
        $data=$request->except('password');
        }
        




        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($request->hasFile('ar_logo')){
        $data['ar_logo']= $this->savePhoto($request->file('ar_logo'));
        }
        if($request->hasFile('en_logo')){
            $data['en_logo']= $this->savePhoto($request->file('en_logo'));
        }

        $school->update($data);
       Session::flash('flash_message', 'تم التعديل بنجاح!');
    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    School::destroy($id);
    return redirect()->back();
    }

}
