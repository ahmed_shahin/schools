<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;


use View;
use App\School;
use App\Day;
use App\Workday;
use App\SchoolGrades;
use App\Staff;
use Validator;
use Session;

class SchoolGradesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = SchoolGrades::latest()->get();
        return view('admin.grades.index',  compact('grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools=['' => 'اختر مدرسة'] + School::pluck('ar_name', 'id')->all();
        $dayes = Workday::pluck('name','id')->all();
        //dd($dayes);
        //dd($schools);
        return view('admin.grades.create',compact('schools','dayes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'manager' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'office' => 'required',
        'transfer' => 'required',
        'school_id'=> 'required',
        'from' => 'required',
        'to' => 'required',
        'lesons' => 'required|numeric',
        'break'=> 'required|numeric'
        ]);

        // if ($validator->fails()) {
        //  return redirect()->back()->with('errors' , $validator->errors());
        // }

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->except('workday');
        //dd($inputs);
        
        //dd($data);

        

        $grade = new SchoolGrades($inputs);
        //dd($grade);
        if ($grade->save()) {

            $data = Input::get('workday');
            foreach ($data as $key => $value) {
                //dd($value);
              $day = new Day([
                  'name' =>$value,
                  'grade_id' => $grade->id
              ]);
              $day->save();
            }
            
           // return redirect('school/addLesons/'.$grade->id);
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $grades = SchoolGrades::find($id);

       return View::make('admin.grades.show',compact('grades'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grades=SchoolGrades::find($id);
         $schools=School::pluck('ar_name', 'id');

          $dayes = Workday::pluck('name','id')->all();

        return View::make('admin.grades.edit',compact('schools','grades','dayes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grades = SchoolGrades::find($id);

        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'manager' => 'required',
        'phone' => 'required|numeric',
        'email' => 'required',
        'office' => 'required',
        'transfer' => 'required',
        'school_id'=> 'required',
        'from' => 'required',
        'to' => 'required',
        'lesons' => 'required|numeric',
        'break'=> 'required|numeric'
        ]);

        if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();
        $grades->update($inputs);
        $data = Input::get('workday');
            foreach ($data as $key => $value) {
                //dd($value);
             $day = Day::where('grade_id',$id)->get();
             foreach ($day as $key => $oneday) {
                 # code...
                $oneday->update([
                  'name' =>$value,
                  'grade_id' => $grade->id
                  ]);
             }
             
            }
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grades = SchoolGrades::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }

    public function addLesons($id)
    {   
        $grade = SchoolGrades::find($id);
        $staffs=['' => 'اختر مدرس'] + Staff::pluck('ar_name', 'id')->all();
        $dayes=['' => 'اختر اليوم'] + Day::pluck('name', 'id')->all();
        $lesonsID = $grade->lesons;
        $gradeId = $grade->id;
         return view('admin.grades.lesons',compact('lesonsID','gradeId','staffs','grade','dayes'));
    }
}
