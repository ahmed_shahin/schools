<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\Http\Requests;
use View;
use Illuminate\Support\Facades\Redirect;
use App\Album;
use App\AlbumPhoto;
use App;
use File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AlbumPhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

 protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(20) . '.' . $photo->guessClientExtension();
        $destinationPath ='uploads/';
        $photo->move($destinationPath, $fileName);
        return 'uploads/'.$fileName;
    }
  

    public function index()
    {
        $album_photos = AlbumPhoto::all();
       return View::make('admin.album_photos.index',compact('album_photos'));

    }


    public function create()
    {
    $countries =['' => 'اختر البوم'] + Album::pluck('name', 'id')->all();
        return View::make('admin.album_photos.create',compact('countries'));
    }   

     public function store(Request $request)
    {    

   $validator = Validator::make($request->all(), [
        'album_id' => 'required',
        'photo' => 'required',


        ]);

        if ($validator->fails()) {
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $img= $this->savePhoto($request->file('photo'));
        $data =AlbumPhoto::create([
        'photo' => $img,
        'album_id' => $request->input('album_id'),
        ]);  
       Session::flash('flash_message', 'تمت الاضافة بنجاح');

    return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $album_photo=AlbumPhoto::find($id);
        $album = Album::pluck('name','id');
       return View::make('admin.album_photos.edit',compact('album_photo','album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        'photo' => 'required',
        'album_id' => 'required',
    
        ]);

        if ($validator->fails()) {
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $img= $this->savePhoto($request->file('photo'));

        $data =AlbumPhoto::where('id',$id)->update([
        'photo' => $img,
        'album_id' => $request->input('album_id')]);  
       Session::flash('flash_message', 'تم  التعديل بنجاح !');


    return redirect()->back();
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    AlbumPhoto::where('id', $id)->delete();

        return redirect()->back();


    }

}
