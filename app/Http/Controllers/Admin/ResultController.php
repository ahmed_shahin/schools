<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\SchoolGrades;
use App\Result;
use App\Classroom;
use App\Semester;
use App\Subject;
use App\Test;
use App\User;
use App\Student;
use Validator;
use Session;


class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Result::latest()->get();
        return view('admin.results.index',  compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grades=['' => 'اختر المرحلة'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $rooms=['' => 'اختر الفصل'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل'] + Semester::pluck('ar_name', 'id')->all();
        $tests=['' => 'اختر الاختبار'] + Test::pluck('name', 'id')->all();

        //dd($schools);
        return view('admin.results.create',compact('grades','rooms','semesters','tests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make(request()->all(), [
        
        'grade_id' => 'required',
        'room_id' => 'required',
        'semester_id' => 'required',
        'test_id' => 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

        $result = new Result($inputs);
        //dd($grade);
        if ($result->save()) {
            return redirect('school/addResults/'.$result->id);
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $results = Result::find($id);

        $grades=['' => 'اختر المرحلة'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $subjects=['' => 'اختر المادة'] + Subject::pluck('ar_name', 'id')->all();
        $staffs=['' => 'اختر المدرس'] + Staff::pluck('ar_name', 'id')->all();
        $rooms=['' => 'اختر الفصل'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل'] + Semester::pluck('ar_name', 'id')->all();
        $lesons=['' => 'اختر الحصة'] + Leson::pluck('name', 'id')->all();
        $dayes=['' => 'اختر اليوم'] + Day::pluck('name', 'id')->all();
        
         return View::make('admin.results.edit',compact('grades','subjects','staffs','rooms','results'),'semesters','dayes');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $results = Result::find($id);

         $validator = Validator::make(request()->all(), [
        
        'grade_id' => 'required',
        'room_id' => 'required',
        'semester_id' => 'required',
        'test_id' => 'required'
        ]);

         if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();
        
        $results->update($inputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $result = Result::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }

    public function addResults($id)
    {   $result = Result::find($id);
        $students = Student::where('semester_id',$result->semester_id)->get();
       // dd($students[0]['name']);
        $count = count($students);
        $resultID = $result->id;
        //dd($resultID);
      
         return view('admin.results.createresult',compact('students','count','resultID'));
    }
}
