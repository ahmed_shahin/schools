<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\Staff;
use App\JobTitles;
use App\Subject;
use App\Nationality;
use App\City;
use App\Country;
use App\Language;
use Validator;
use Session;
use File;

class StaffsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $staffs = Staff::latest()->get();
        return view('admin.staffs.index',  compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobtitles=['' => 'اختر المسمي الوظيفي'] + JobTitles::pluck('ar_name', 'id')->all();
        $subjects=['' => 'اختر المادة'] + Subject::pluck('ar_name', 'id')->all();
        $nationalities=['' => 'اختر الجنسية'] + Nationality::pluck('ar_name', 'id')->all();
        $cities=['' => 'اختر المدينة'] + City::pluck('name', 'id')->all();
        $countries=['' => 'اختر الدولة'] + Country::pluck('name', 'id')->all();
        $genders=['' => 'اختر النوع'] + [0 => 'ذكر', 1 => 'انثى'];
        $languages=['' => 'اختر اللغة الام'] + Language::pluck('ar_name', 'id')->all();

        //dd($schools);
        return view('admin.staffs.create',compact('jobtitles','subjects','nationalities','cities','countries','genders','languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'jobtitle_id' => 'required',
        'phone' => 'required',
        'place' => 'required',
        'card' => 'required',
        'email'=> 'required',
        'type' => 'required',
        'subject_id' => 'required',
        'nationality_id' => 'required',
        'language_id' => 'required',
        'country_id'=> 'required',
        'city_id' => 'required',
        'username' => 'required',
        'password' => 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

    
        $inputs['password'] = bcrypt($inputs['password']);

        $staff = new Staff($inputs);
        //dd($grade);
        if ($staff->save()) {
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $staffs = Staff::find($id);

       return View::make('admin.staffs.show',compact('staffs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staffs = Staff::find($id);
        $jobtitles=['' => 'اختر المسمي الوظيفي'] + JobTitles::pluck('ar_name', 'id')->all();
        $subjects=['' => 'اختر المادة'] + Subject::pluck('ar_name', 'id')->all();
        $nationalities=['' => 'اختر الجنسية'] + Nationality::pluck('ar_name', 'id')->all();
        $cities=['' => 'اختر المدينة'] + City::pluck('name', 'id')->all();
        $countries=['' => 'اختر الدولة'] + Country::pluck('name', 'id')->all();
        $genders=['' => 'اختر النوع'] + [0 => 'ذكر', 1 => 'انثى'];
        $languages=['' => 'اختر اللغة الام'] + Language::pluck('ar_name', 'id')->all();

        return View::make('admin.staffs.edit',compact('staffs','jobtitles','subjects','nationalities','cities','countries','genders','languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $staffs = Staff::find($id);

         $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'jobtitle_id' => 'required',
        'phone' => 'required',
        'place' => 'required',
        'card' => 'required',
        'email'=> 'required',
        'type' => 'required',
        'subject_id' => 'required',
        'nationality_id' => 'required',
        'language_id' => 'required',
        'country_id'=> 'required',
        'city_id' => 'required',
        'username' => 'required',
        'password' => 'required'
        ]);

        if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();

        $inputs['password'] = bcrypt($inputs['password']);
        $staffs->update($inputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staffs = Staff::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
