<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SchoolsLoginController extends Controller
{   
    public function __construct()
    {
        $this->middleware('guest:school')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.schools.login');

    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if(Auth::guard('school')->attempt(['email'=> $request->email,'password' => $request->password ],$request->remember)){
            
                  return redirect('school');
        }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    
    public function logout()
    {
        Auth::guard('school')->logout();
        return redirect('school/login');

    }

}
