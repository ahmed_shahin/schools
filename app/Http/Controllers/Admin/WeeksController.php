<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


use View;
use App\Week;

use Validator;
use Session;

class WeeksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $weeks = Week::latest()->get();
        return view('admin.weeks.index',  compact('weeks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.weeks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'from' => 'required',
        'to' => 'required'
        ]);

        // if ($validator->fails()) {
        //  return redirect()->back()->with('errors' , $validator->errors());
        // }

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();
        

        $week = new Week($inputs);
        //dd($grade);
        if ($week->save()) {
            
           // return redirect('school/addLesons/'.$grade->id);
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $weeks = Week::find($id);

       return View::make('admin.weeks.show',compact('weeks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $weeks=Week::find($id);
         

        return View::make('admin.weeks.edit',compact('weeks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $weeks = Week::find($id);

        $validator = Validator::make(request()->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'from' => 'required',
        'to' => 'required'
        ]);

        if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();
        $weeks->update($inputs);
        
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $weeks = Week::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
