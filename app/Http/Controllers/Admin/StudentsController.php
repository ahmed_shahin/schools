<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\Classroom;
use App\Semester;
use App\SchoolGrades;
use App\Subject;
use App\Student;
use App\Master;
use Validator;
use Session;
use File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class StudentsController extends Controller
{
    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(20) . '.' . $photo->guessClientExtension();
        $destinationPath ='uploads/';
        $photo->move($destinationPath, $fileName);
        return 'uploads/'.$fileName;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::latest()->get();
        return view('admin.students.index',  compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms=['' => 'اختر الصف الدراسي'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل الدراسي'] + Semester::pluck('ar_name', 'id')->all();
        $grades=['' => 'اختر المرحلة الدراسية'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $genders=['' => 'اختر النوع'] + [0 => 'ذكر', 1 => 'انثى'];

        //dd($schools);
        return view('admin.students.create',compact('rooms','semesters','grades','genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
        'name' => 'required',
        'img' => 'required',
        'birthdate' => 'required',
        'enrollmentdate' => 'required',
        'gender' => 'required',
        'grade_id' => 'required',
        'classroom_id' => 'required',
        'semester_id'=> 'required',
        'fathername' => 'required',
        'phone'=> 'required',
        'email' => 'required',
        'relation_type'=> 'required',
        'country_id'=> 'required',
        'city_id'=> 'required',
        'street' => 'required',
        'username'=> 'required',
        'password' => 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request(['name','img','birthdate','enrollmentdate','gender','grade_id','classroom_id','semester_id']);

       
        if($request->hasFile('img')){
        $inputs['img']= $this->savePhoto($request->file('img'));
        }
        
        if($inputs['gender'] == "0"){
            $inputs['gender'] = 'ذكر';
        }

        if($inputs['gender'] == "1"){
            $inputs['gender'] = 'انثى';
        }

        $student = new Student($inputs);
        //dd($student);
        
        $fatherinputs = request(['fathername','phone','email','relation_type','country_id','city_id','street','username','password']);

        $fatherinputs['password'] = bcrypt($fatherinputs['password']);
       
        $student->save();
        $father = new Master($fatherinputs);

        $father->student_id = $student->id;
         //dd($father);
        if ($father->save()) {
            //$father->save();
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $students = Student::find($id);

       return View::make('admin.students.show',compact('students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students=Student::find($id);
        $father_id = Master::where('student_id',$id)->pluck('id')->first();
        //dd($father_id);
        $father = Master::find($father_id);
        $rooms=['' => 'اختر الصف الدراسي'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل الدراسي'] + Semester::pluck('ar_name', 'id')->all();
        $grades=['' => 'اختر المرحلة الدراسية'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $genders=['' => 'اختر النوع'] + [0 => 'ذكر', 1 => 'انثى'];

        return View::make('admin.students.edit',compact('students','semesters','rooms','grades','genders','father'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $students = Student::find($id);
        $father_id = Master::where('student_id',$id)->pluck('id')->first();
        //dd($father_id);
        $father = Master::find($father_id);

        $validator = Validator::make(request()->all(), [
        'name' => 'required',
        'img' => 'required',
        'birthdate' => 'required',
        'enrollmentdate' => 'required',
        'gender' => 'required',
        'grade_id' => 'required',
        'classroom_id' => 'required',
        'semester_id'=> 'required',
        'fathername' => 'required',
        'phone'=> 'required',
        'email' => 'required',
        'relation_type'=> 'required',
        'country_id'=> 'required',
        'city_id'=> 'required',
        'street' => 'required',
        'username'=> 'required',
        'password' => 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request(['name','img','birthdate','enrollmentdate','gender','grade_id','classroom_id','semester_id']);

       
        if($request->hasFile('img')){
        $inputs['img']= $this->savePhoto($request->file('img'));
        }

        if($inputs['gender'] == "0"){
            $inputs['gender'] = 'ذكر';
        }

        if($inputs['gender'] == "1"){
            $inputs['gender'] = 'انثى';
        }

        $fatherinputs = request(['fathername','phone','email','relation_type','country_id','city_id','street','username','password']);

        $fatherinputs['password'] = bcrypt($fatherinputs['password']);

        $students->update($inputs);
        $father->update($fatherinputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $students = Student::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
