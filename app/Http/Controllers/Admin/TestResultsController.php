<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\SchoolGrades;
use App\Result;
use App\Classroom;
use App\Semester;
use App\Subject;
use App\Test;
use App\User;
use App\Student;
use Validator;
use Session;

class TestResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       for ($i=0; $i < request('count'); $i++) { 
            # code...  
        $validator = Validator::make(request()->all(), [
        'student_id' => 'required',
        'result'.$i => 'required',
        ]);
      
      }

      if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

         for ($j=1; $j < request('count'); $j++) { 
        
            $results = Result::find(request('resultid'));
            //dd(request('resultID'));
            $result = new Result([
              'grade_id' =>$results->grade_id,
              'room_id' => $results->room_id,
              'semester_id' => $results->semester_id,
              'test_id' => $results->test_id,
              'student_id' => request('student_id'),
              'result' => request('result'.$j)
              ]);
            $result->save();
        //dd($grade);
        }
            return redirect('school/results');
            Session::flash('flash_message', 'تمت الاضافة بنجاح!');
            return Redirect::back();   
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
