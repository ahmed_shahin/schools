<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\Http\Requests;
use View;
use Illuminate\Support\Facades\Redirect;
use App\Settings;
use App\About;
use App\Feature;
use App;
use Auth;
class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
        


   public function home()
    {
    return View::make('admin.home');

    } 

    public function about_us()
    {
       $about_us=About::first();
       return View::make('admin.settings.about_us',compact('about_us'));   
    }

 

     public function update_about_us(Request $request, $id)
    {    
        $data=$request->all();
        $validator = Validator::make($request->all(), [
        'details' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
         About::where('id',$id)->update([
        'details' => $request->input('details'),

            ]); 
         Session::flash('flash_message', 'تم التعديل بنجاح!');
         return redirect()->back();
     } 

       public function features()
    {
       $features=Feature::first();
        return view('admin.settings.features',compact('features'));
    } 
    
     public function update_features(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
       'ar_details' => 'required',
        'en_details' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
         Feature::where('id',$id)->update([
        'ar_details' => $request->input('ar_details'),
        'en_details' => $request->input('en_details'),
]); 
        Session::flash('flash_message', 'تم التعديل بنجاح!');
 
    return redirect()->back();
     } 


     public function index()
    {
       $settings=Settings::first();
       return View::make('admin.settings.index',compact('settings'));

    }


    public function create()
    {
    return View::make('admin.settings.create');

    }   

     public function store(Request $request)
    {    
    $data=$request->all();
    $validator = Validator::make($request->all(), [
        'about_us' => 'required',
        'goals' => 'required',
        'manager_letter' => 'required',
        'mission' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $data['school_id']=Auth::guard('school')->user()->id;
        $settings=Settings::where('school_id',Auth::guard('school')->user()->id)->first();
        if ($settings) {
            Session::flash('edit_message', 'تم ادخال البيانات سابقا .. يرجى تعديلها من هنا');
             return redirect('school/settings/edit');
        }
        Settings::create($data);  
        Session::flash('flash_message', 'تمت الاضافة بنجاح');
    return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settings=Settings::where('school_id',Auth::guard('school')->user()->id)->first();
       return View::make('admin.settings.edit',compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {

     $settings=Settings::find($id);
     $data=$request->all();
     $settings->update($data);  
    Session::flash('flash_message', 'تم الحفظ بنجاح');
    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Settings::destroy($id);
    return redirect()->back();
    }

}
