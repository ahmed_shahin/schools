<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use View;
use App\SchoolGrades;
use App\Classroom;
use App\Semester;
use App\Test;
use App\Day;
use Validator;
use Session;

class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::latest()->get();
        return view('admin.tests.index',  compact('tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grades=['' => 'اختر المرحلة'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $rooms=['' => 'اختر الفصل'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل'] + Semester::pluck('ar_name', 'id')->all();
        $dayes=['' => 'اختر اليوم'] + Day::pluck('name', 'id')->all();

        //dd($schools);
        return view('admin.tests.create',compact('grades','rooms','semesters','dayes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
        'date' => 'required',
        'grade_id' => 'required',
        'room_id' => 'required',
        'semester_id' => 'required',
        'day_id' => 'required',
        'name' => 'required'
        ]);

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $inputs = request()->all();

        $test = new Test($inputs);
        //dd($grade);
        if ($test->save()) {
            Session::flash('flash_message', 'تمت الاضافة ميعاد اختبار!');
            return Redirect::back();   
        }
        return back()->with('error', 'حدث خطأ حاول مرة اخري');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tests = Test::find($id);

       return View::make('admin.tests.show',compact('tests'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $tests = Test::find($id);

         

        $grades=['' => 'اختر المرحلة'] + SchoolGrades::pluck('ar_name', 'id')->all();
        $rooms=['' => 'اختر الفصل'] + Classroom::pluck('ar_name', 'id')->all();
        $semesters=['' => 'اختر الفصل'] + Semester::pluck('ar_name', 'id')->all();
        $dayes=['' => 'اختر اليوم'] + Day::pluck('name', 'id')->all();

        return View::make('admin.tests.edit',compact('tests','grades','rooms','semesters','dayes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $tests = Test::find($id);

          $validator = Validator::make(request()->all(), [
        'date' => 'required',
        'grade_id' => 'required',
        'room_id' => 'required',
        'semester_id' => 'required',
        'day_id' => 'required',
        'name' => 'required'
        ]);

            if ($validator->fails()) {
         return redirect()->back()->with('errors' , $validator->errors());
        }

        $inputs = request()->all();
        
        $tests->update($inputs);
        Session::flash('flash_message', 'تم التعديل بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $tests = Test::find($id)->delete();
        Session::flash('flash_message', 'تم المسح بنجاح!');
        return redirect()->back();
    }
}
