<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\Http\Requests;
use View;
use Illuminate\Support\Facades\Redirect;
use App\Company;
use File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class CompaniesController extends Controller
{

   protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(20) . '.' . $photo->guessClientExtension();
        $destinationPath ='uploads/';
        $photo->move($destinationPath, $fileName);
        return 'uploads/'.$fileName;
    }



       public function index()
    {
       $companies=Company::get();
       return View::make('admin.companies.index',compact('companies'));

    }

    

    public function create()
    {
    return View::make('admin.companies.create');

    }   

     public function store(Request $request)
    {    
        $data=$request->all();
        $validator = Validator::make($request->all(), [
        'ar_name' => 'required|unique:companies',
        'en_name' => 'required|unique:companies',
        'commercial_no' => 'required',
        'ar_logo' => 'required',
        'en_logo' => 'required',
        'country' => 'required',
        'city' => 'required',
        'street' => 'required',
        'location_details' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($request->hasFile('ar_logo')){
        $data['ar_logo']= $this->savePhoto($request->file('ar_logo'));
        }
        if($request->hasFile('en_logo')){
            $data['en_logo']= $this->savePhoto($request->file('en_logo'));
        }
        if($request->hasFile('contract')){
            $data['contract']= $this->savePhoto($request->file('contract'));
        }
        if($request->hasFile('commercial_license')){
            $data['commercial_license']= $this->savePhoto($request->file('commercial_license'));
        }
        if($request->hasFile('bank_data')){
            $data['bank_data']= $this->savePhoto($request->file('bank_data'));
        }
        Company::create($data);
       Session::flash('flash_message', 'تمت الاضافة بنجاح!');

   return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     $company=Company::find($id);

       return View::make('admin.companies.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company=Company::find($id);

       return View::make('admin.companies.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
    	$company = Company::find($id);
              $data=$request->all();
        $validator = Validator::make($request->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'commercial_no' => 'required',
        'country' => 'required',
        'city' => 'required',
        'street' => 'required',
        'location_details' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($request->hasFile('ar_logo')){
        $data['ar_logo']= $this->savePhoto($request->file('ar_logo'));
        }
        if($request->hasFile('en_logo')){
            $data['en_logo']= $this->savePhoto($request->file('en_logo'));
        }
        if($request->hasFile('contract')){
            $data['contract']= $this->savePhoto($request->file('contract'));
        }
        if($request->hasFile('commercial_license')){
            $data['commercial_license']= $this->savePhoto($request->file('commercial_license'));
        }
        if($request->hasFile('bank_data')){
            $data['bank_data']= $this->savePhoto($request->file('bank_data'));
        }
        $company->update($data);
       Session::flash('flash_message', 'تم التعديل بنجاح!');
    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $company=Company::find($id);
    $company->schools->delete();
    return redirect()->back();
    }

}
