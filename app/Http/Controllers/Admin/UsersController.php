<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\Http\Requests;
use View;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\City;
use App\CardType;
use App\Section;
use App;

use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

       public function index()
    {
       $users=User::where('type',"shop")->get();
       return View::make('admin.users.index',compact('users'));

    }

    

    public function create()
    {
        $cities= [''=>'اختر مدينة'] + City::lists('ar_name','id')->all();
        $sections=  [''=>'اختر قسم'] + Section::lists('ar_name','id')->all();
    return View::make('admin.users.create',compact('cities','sections'));

    }   

     public function store(Request $request)
    {   $data = $request->all();  
        $validator = Validator::make($data, [
        'ar_name' => 'required',
        'en_name' => 'required',
        'email' => 'required|email|unique:users',
        'phone' => 'required|numeric',
        'password' => 'required|min:6|confirmed',
        'password_confirmation' => 'required|min:6',
        'photo' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
       $file = $request->file('photo');
        $destinationPath = 'uploads/'; 
        $extension = $file->getClientOriginalExtension();
        $image=str_random(10).".".$extension;
        $file->move($destinationPath , $image);
        $data['photo']='uploads/'.$image;
        $data['type']='shop';
        $data['password']=Hash::make($request->password);
       User::create($data); 
       Session::flash('flash_message', 'تمت الاضافة بنجاح!');

   return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$sections=Section::all();
    	$cities=City::all();
        $user=User::find($id);

       return View::make('admin.users.edit',compact('user','sections','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
    	$user = User::findorfail($id);
        $validator = Validator::make($request->all(), [
        'ar_name' => 'required',
        'en_name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'password' => 'min:6|confirmed',
        'password_confirmation' => 'min:6',
        ]);

        if ($validator->fails()) {
            return redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
         if($request->hasFile('photo')){
        $file = $request->file('photo');
        $destinationPath = 'uploads/'; 
        $extension = $file->getClientOriginalExtension();
        $image=str_random(10).".".$extension;
        $file->move($destinationPath , $image);
       $photo = 'uploads/'.$image;
       }else{
       $photo = User::where('id',$id)->value('photo');
           }
       if (!$request->password =="") {
       $data = $request->all(); 
       $data['photo']=$photo;
       $data['password']=Hash::make($request->password);
       $user->update($data);
       }else{
       $data = $request->except(['password']); 
       $data['photo']=$photo;
       $user->update($data);
       }
       Session::flash('flash_message', 'تم التعديل بنجاح!');
    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    User::destroy($id);
    return redirect()->back();
    }

}
