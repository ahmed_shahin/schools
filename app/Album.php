<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model {

       protected $fillable = [
        'name'
    ];
    public function album_photos() {
    	return $this->hasMany('App\AlbumPhoto', 'album_id', 'id');
}

}
